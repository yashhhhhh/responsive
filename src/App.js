import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import configureStore from './store';
import posed, {PoseGroup} from 'react-pose'
import './assets/style.scss'
import trainerRoutes from './applications/trainers/routes'
import authRoutes from './applications/auth/routes'
import Favicon from 'react-favicon';
const RouteContainer = posed.div({
  enter: { opacity: 1, delay: 200, beforeChildren: 300 },
  exit: { opacity: 0 }
});

let store = configureStore()

function App() {
  return (
    <div className="App">
      <Favicon url="https://upschool-sgp.sgp1.cdn.digitaloceanspaces.com/others/assets/images/favicon.png" />
      <Router>
        <Provider store={store}>
          <Route render={({location}) => (
           // <PoseGroup>
                <RouteContainer key={location.pathname}>
                  <Switch>
                    { [...authRoutes,...trainerRoutes].map(({path, component, key}, index) =>
                      <Route exact path={path}
                        key={key} component={component}
                      />
                    )}
                  </Switch>
                </RouteContainer>
           //</PoseGroup>
            )}>
          </Route>
        </Provider>
      </Router>
    </div>
  );
}

export default App;
