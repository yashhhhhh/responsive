import config from '../config'

export const storagePath = config.storage.storageAddress
export const quesTypes = {
  "1" : 'Multi-Choice',
  "2" : "Descriptive"
}

export const optionNumbers = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
