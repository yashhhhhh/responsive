import axios from 'axios';
import config from '../config'
import { getStore } from '../store/globalStore'
import { SHOW_TOAST } from '../applications/auth/actions'
var axiosInstance = axios.create();
axiosInstance.defaults.baseURL = config.api.networkInterface
axiosInstance.interceptors.request.use(function(config){
  if(localStorage.getItem('userInfo')){
    config['headers']['Authorization'] = JSON.parse(localStorage.getItem('userInfo')).access
  }
  return config
},function(error){
  return Promise.reject(error)
})

axiosInstance.interceptors.response.use(function(response){
  if(response.data.statusCode >= 200 && response.data.statusCode < 300){
    return response
  }else if(response.data.statusCode === 1001){
    localStorage.clear()
    window.location.assign('/login')
    let store = getStore();
    store.dispatch({
      type: SHOW_TOAST,
      payload: {
        message: "Your session has expired, please login again",
        type: 'error',
        flag: true
      }
    });
    return response
  }else{
    let store = getStore();
    if(!window.location.href.includes('login')){
      store.dispatch({
        type: SHOW_TOAST,
        payload: {
          message: response.data.message,
          type: 'error',
          flag: true
        }
      });
    }
    return response
  }
},function(error){
  let store = getStore();
  store.dispatch({
    type: SHOW_TOAST,
    payload: {
      message: "Unable to process due to technical error.",
      type: 'error',
      flag: true
    }
  });
  return Promise.reject(error)
})

export default axiosInstance
