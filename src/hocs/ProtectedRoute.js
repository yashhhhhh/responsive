import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

export default function ProtectedRoute(HocComponent){
  class ProtectedRouter extends Component{
    componentWillMount(){
      console.log(this.props)
      if(!localStorage.getItem('userInfo') && !localStorage.getItem('syllabus')) {
        this.props.history.push('/login')
      }
    }

    render(){
      if(localStorage.getItem('userInfo')) {
        // if(localStorage.getItem('syllabus')){
          return(
            <HocComponent props={this.props}/>
          )
        // }else{
        //   this.props.history.push('/select-class')
        // }
      }else{
        this.props.history.push('/login')
      }
      return(<div></div>)
    }
  }
  return withRouter(ProtectedRouter)
}
