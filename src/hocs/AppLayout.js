import React from 'react'
import { connect } from 'react-redux'
import { MobileHeader, SideBar, DesktopHeader, Footer } from '../shared_elements/components'
import { isMobile } from 'react-device-detect';
import * as actions from '../applications/auth/actions'
import { globalGetService } from '../global_api_services'
import { Snackbar, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';

export default function AppLayout(HocComponent){
  class AppLayout extends React.Component{
    constructor(props) {
      super(props);
      this.state={
        open: false
      }
    }

    // componentDidMount(){
      
    //   globalGetService('teacher/standards/')
    //   .then(response => {
    //     this.props.standardList(response.data.data)
    //   })
    // }

    toggleDrawer = (flag) =>{
      this.setState({
        open: flag
      })
    }
    render(){
      return(
        <div>
          { isMobile ? <MobileHeader open={this.state.open} standards={this.props.standards} toggleDrawer={this.toggleDrawer}/> :<DesktopHeader/> }
          { !isMobile ? <SideBar standards={this.props.standards}/> :null }
          <main style={{marginLeft: !isMobile ? '230px' : '0px', marginTop: '60px', marginBottom: '90px'}}>

            <div className="main-content" style={{padding: '25px 30px'}}>
              <HocComponent {...this.props}/>
            </div>
          </main>
          <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={this.props.toast.flag}
              autoHideDuration={3000}
              onClose={this.props.closeToast}
              className={this.props.toast.type == "error" ? "warning-toast" : "success-toast"}
              ContentProps={{
                'aria-describedby': 'message-id',
              }}
              message={<span id="message-id">{this.props.toast.message}</span>}
              action={[
                <IconButton
                  key="close"
                  aria-label="close"
                  color="inherit"
                  onClick={this.props.closeToast}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
            />
          <Footer />
        </div>
      )
    }
  }
  const mapStateToProps = (state) =>({
    standards: state.AuthReducer.standards,
    toast: state.AuthReducer.toast,
  })
  const mapDispatchToProps = (dispatch) =>{
    return{
      standardList:(data) => dispatch({
        type: actions.STANDARDS_LIST,
        payload: data
      }),
      closeToast: () => dispatch({
        type: actions.SHOW_TOAST,
        payload: {
          message: '',
          type: '',
          flag: false
        }
      })
    }
  }
  return connect(mapStateToProps, mapDispatchToProps)(AppLayout)
}
