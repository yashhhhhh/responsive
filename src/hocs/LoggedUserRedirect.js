import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
function LoggedUserRed(ComposedComponent, extraInfo) {
 class LoginAccessCrtl extends Component {
   componentWillMount(){
     let userDetail = JSON.parse(localStorage.getItem('userInfo'));
     let standardDetail = JSON.parse(localStorage.getItem('syllabus'));
     if(userDetail && standardDetail){
       this.props.history.push( "/dashboard" );
     }
   }
   render() {
     if(!localStorage.getItem('userInfo') && !localStorage.getItem('syllabus')) {
       return(<ComposedComponent/>)
     }else {
       return null
     }
   }
 }
  return withRouter(LoginAccessCrtl);
}
export default LoggedUserRed;
