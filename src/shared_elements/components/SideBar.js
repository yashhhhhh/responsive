import React from 'react'
import { Link } from 'react-router-dom'
import SidebarPrimaryLink from './SidebarPrimaryLink'
import List from '@material-ui/core/List'
import { storagePath } from '../../constants'
import { ClassSwitcher } from '../../applications/trainers'
import { isMobile } from 'react-device-detect';
import { Button } from '@material-ui/core'

export default function SideBar(props){
    let location = window.location.href
    {
      // {className: location.includes('announcement') ? 'active' : '',title: 'Announcement', path: '/announcement', iconPath: location.includes('announcement') ? 'announcer_blue.svg' : 'announcer.svg'},
    }
    return(
      <div className="sidebar">
        <div className="app-logo">
          <Link to="/dashboard">
            <img src={storagePath + 'logo.png'} alt="" />
          </Link>
        </div>
        <div className="sidebar-content hidden-scrollbar" style={{height: isMobile ? window.innerHeight - 190 : window.innerHeight - 60}}>
          <div className="primary-menu class-switcher">
            <ClassSwitcher standards={props.standards}/>
          </div>
          <span className="sidebar-heading">Main Menu</span>
          <div className="primary-menu">
              <List component="nav" aria-label="main mailbox folders">
                {[
                  {className: location.includes('dashboard') ? 'active' : '',title: 'Dashboard', path: '/dashboard', iconPath: location.includes('dashboard') ? 'home_blue.svg' : 'home.svg'},
                  {className: location.includes('students') ? 'active' : '',title: 'Teacher', path: '/students', iconPath: location.includes('students') ? 'users_blue.svg' : 'users.svg'},               
                  {className: location.includes('syllabus') ? 'active' : '',title: 'Syllabus', path: '/syllabus', iconPath: location.includes('syllabus') ? 'books_blue.svg' : 'books.svg'},
                  {className: location.includes('class_test') ? 'active' : '',title: 'Classroom', path: '/class_test', iconPath: location.includes('class_test') ? 'puzzle-piece-blue.svg' : 'puzzle-piece.svg'},
                  // {className: location.includes('Classroom') ? 'active' : '',title: 'Classroom', path: '/Classroom', iconPath: location.includes('Classroom') ? 'puzzle-piece-blue.svg' : 'puzzle-piece.svg'},
                ].map((link, index) =>
                  <SidebarPrimaryLink link={link} key={index}/>
                )}
              </List>
          </div>
          <Button  className="button1" variant="contained"> A </Button>
        </div>
      </div>
    )
}
