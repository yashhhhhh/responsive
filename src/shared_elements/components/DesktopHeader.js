import React from 'react'
import InputBase from '@material-ui/core/InputBase';
import Grid from '@material-ui/core/Grid';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { getStudentsList } from '../../applications/trainers/api_services'
import Avatar from '@material-ui/core/Avatar';
import { storagePath } from '../../constants'
import { withRouter } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
// import Autocomplete from '@material-ui/lab/Autocomplete';

class DesktopHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      userMenu: false,
      search: ''
    }

    this.getStudentsList = getStudentsList.bind(this)
  }

  logout() {
    localStorage.clear()
    window.location.assign('/login')
  }

  componentDidMount() {
    if (this.props.location.search) {
      this.setState({ search: this.props.location.search.split('?search=')[1] })
    }
  }

  searchStudent = (evt) => {
    evt.preventDefault()
    evt.stopPropagation()
    this.props.history.push('/students?search=' + evt.target.student.value)
  }
 

  render() {
    const user = JSON.parse(localStorage.getItem('userInfo')).user
    // const top100Films = [
    //   { title: 'The Shawshank Redemption', year: 1994 },
    //   { title: 'The Godfather', year: 1972 },
    //   { title: 'The Godfather: Part II', year: 1974 },
    //   { title: 'The Dark Knight', year: 2008 }
    // ]
    return (
      <div className="desktop-header">
        <div className="header-main-content">
          <form onSubmit={this.searchStudent}>
            <InputBase
              id="Teacher"
              name="Teacher"
              onChange={(evt) => this.setState({ search: evt.target.value })}
              value={this.state.search}
              className="search-input"
              startAdornment={<InputAdornment position="start"><Search /></InputAdornment>}
              placeholder="Search for Teacher"
            />
            <span>
            {/* <Autocomplete className="search"
            id="combo-box-demo"
            options={top100Films}
            getOptionLabel={(option) => option.title}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label=" Search for classroom" variant="outlined" />}
          /> */}
          </span>
          </form>
          
        </div>
        <div className="header-user-info" onClick={() => this.setState({ userMenu: !this.state.userMenu })}>
          <ClickAwayListener onClickAway={() => this.setState({ userMenu: false })}>
            <div>
              <Grid container spacing={1} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Grid item xs={3} className="user-image"><Avatar>{user.full_name.substr(0, 1)}</Avatar></Grid>
                <Grid item xs={9} className="user-detail">
                  <div> <p> Hi  Principal </p>
                     {/* {user.full_name.length > 15 ? user.full_name.substr(0, 10) + '...' : user.full_name} */}
                     </div>
                  <div style={{ display: 'none' }}>Last Login: 2 days Ago</div>
                  <img src={storagePath + "settings.png"} alt="user" />
                </Grid>
              </Grid>
              {this.state.userMenu ? (
                <Paper>
                  <Button onClick={() => this.logout()} className="user-action">Logout</Button><br />
                  <Button style={{ display: 'none' }} className="user-action">Settings</Button>
                </Paper>
              ) : null}
            </div>
          </ClickAwayListener>
        </div>
      </div>
    )
  }
}

export default withRouter(DesktopHeader)
