import React from 'react'
import { storagePath } from '../../constants'
export default function NoRecordFound(props){
  return(
    <div className="no-record-found" style={props.style}>
      <img src={props.image ? props.image : storagePath + "blackboard.svg"} alt="no record found"/>
      <h4>{props.title ? props.title : 'No Records Found'}</h4>
      <p>{props.content ? props.content : ''}</p>
      <div>
      {props.component}
      </div>
    </div>
  )
}
