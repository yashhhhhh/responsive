import React from 'react'
import Drawer from '@material-ui/core/Drawer';
import SideBar from './SideBar'
import MenuIcon from '@material-ui/icons/Menu';

export default class MobileHeader extends React.Component{
    constructor(props){
      super(props)
    }
    render(){
      return(
        <div className="mobile-header">
          <MenuIcon className="menu-icon" onClick={() => this.props.toggleDrawer(true)}/>
          <Drawer
            anchor="left"
            open={this.props.open}
            onClose={() => this.props.toggleDrawer(false)}
          >
            <SideBar standards={this.props.standards}></SideBar>
          </Drawer>
        </div>
      )
    }
  }
