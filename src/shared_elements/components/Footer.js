import React from 'react'

export default class Footer extends React.Component{
    constructor(props){
      super(props)
    }
    render(){
      return(
        <footer className="desktop-footer">
          <p>
            <span>© 2020, UpSchool EdTech Private Limited. All rights reserved.</span>
            <a href="https://www.up.school/terms-and-conditions/" target="_blank">Terms and Conditions</a> <span className="devider">|</span>       
            <a href="https://www.up.school/privacy-policy/" target="_blank">Privacy Policy</a> 
          </p>
        </footer>
      )
    }
  }
