import React from 'react'
import { storagePath } from '../../constants'

export default function Loader(props){
  return(
    <div className="loader">
      <img src={storagePath+"loader.gif"} alt="loading..."/>
    </div>
  )
}
