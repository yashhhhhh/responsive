import MobileHeader from './MobileHeader'
import DesktopHeader from './DesktopHeader'
import SideBar from './SideBar'
import Loader from './Loader'
import NoRecordFound from './NoRecordFound'
import Footer from './Footer'

export {
  MobileHeader,
  DesktopHeader,
  SideBar,
  Loader,
  NoRecordFound,
  Footer
}
