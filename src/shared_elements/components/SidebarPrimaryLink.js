import React, { Fragment } from 'react';
import ListItem from '@material-ui/core/ListItem';
import { withRouter } from 'react-router-dom'
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider'
import { storagePath } from '../../constants'

function SidebarPrimaryLink(props){
  return(
    <Fragment>
      <ListItem button onClick={() => props.history.push(props.link.path)} className={props.link.className}>
        <ListItemIcon>
          <img src={storagePath + props.link.iconPath} alt={props.link.title}/>
        </ListItemIcon>
        <ListItemText className="sidebar-menu-text" primary={props.link.title} />
      </ListItem>
      <Divider style={props.link.path.includes('syllabus') ? {height: '1px'} : {}}/>
    </Fragment>
  )
}

export default withRouter(SidebarPrimaryLink)
