import React from 'react'
export default function StatusData(props){
  return(
    <div className="status-data">
        <h2>{props.count}</h2>
        <p>{props.label}</p>
    </div>
  )
}
