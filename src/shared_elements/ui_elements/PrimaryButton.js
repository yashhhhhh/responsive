import React from 'react'
import Button from '@material-ui/core/Button';

const PrimaryButton = (props) =>{
  return(
    <Button style={props.style} disabled={props.disabled} onClick={props.onClick} value={props.value} type={props.type ? props.type : 'button'} className={`button primary-button${props.className ? " " + props.className : ""}`}>
        {props.value}
    </Button>
  )
}

export default PrimaryButton
