import React from 'react'
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

export default function TabPanel(props){
  const { children, value, index, ...other } = props;
  return(
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {children}
    </Typography>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
