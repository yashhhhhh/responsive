import React from 'react'
import StatusData from './StatusData'
import { withRouter } from 'react-router-dom'
function StudentStatus(props){
  const { status, statusInfo } = props
  // console.log('chapterStatus', status)
  return(
    <div className="student-status">
        <p>{props.title}</p>
        {
          statusInfo && <ul className="status-table">
            {statusInfo.map((item, index)=>{
              if(item.id === "need_attention"){
                return <li key={index} style={{cursor: "pointer"}} onClick={() => props.history.push('/attention')}><StatusData count={status[item.id]} label={item.label}/></li>
              }
              return <li key={index}><StatusData count={status[item.id]} label={item.label}/></li>
            })}
          </ul>
        }
    </div>
  )
}

export default withRouter(StudentStatus)
