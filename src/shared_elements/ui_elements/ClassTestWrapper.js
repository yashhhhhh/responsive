import React from 'react'
import PageHeader from './PageHeader'
import StudentStatus from './StudentStatus'
import { Grid } from '@material-ui/core'
const chapterStudentStatusInfo=[{label: "Attended", id: 'attended'}, {label: "Passed", id: 'passed'}, {label: "Failed", id: 'failed'}]
export default function ClassTestWrapper(props){
    const { classTest, children } = props
    return(
        <div style={{padding:'0px'}} className="classTest-wrapper">
            <Grid container >
                <Grid item xs={7} className="classTest-heading">
                    <PageHeader title={classTest.title} linkInfo={{main: '/class_test', label: 'Class Tests', sub: ''}} />
                </Grid>
                <Grid item xs={5}>
                    <StudentStatus status={props.classTestStatus} statusInfo={chapterStudentStatusInfo} title="Class Test Result"/>
                </Grid>
            </Grid>
            {children}
        </div>
    )
}
