import React from 'react'
import Button from '@material-ui/core/Button';

const SuccessButton = (props) =>{
  return(
    <Button disabled={props.disabled} onClick={props.onClick} type={props.type ? props.type : 'button'} className={`button success-button${props.className ? " " + props.className : ""}`}>
      {props.value}
    </Button>
  )
}

export default SuccessButton
