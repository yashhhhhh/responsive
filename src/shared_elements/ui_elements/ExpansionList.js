import React, { useState } from 'react'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { storagePath } from '../../constants'

export default function ExpansionList(props){
  const [expanded, toggleExpansion] = useState(false)
  return(
    <div>
    <ExpansionPanel square expanded={expanded} onChange={() => props.toggle ? toggleExpansion(!expanded) : null} {...props} title={""}>
      <ExpansionPanelSummary expandIcon={props.toggle ? <ExpandMoreIcon /> : <img src={`${storagePath}lock.svg`}/>} aria-controls="panel1d-content" id="panel1d-header">
        {props.title}
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
          {props.children}
      </ExpansionPanelDetails>
    </ExpansionPanel>
    </div>
  )
}
