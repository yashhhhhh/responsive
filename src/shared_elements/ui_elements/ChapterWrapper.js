import React from 'react'
import PageHeader from './PageHeader'
import StudentStatus from './StudentStatus'
import { Grid } from '@material-ui/core'
import { integer_to_roman } from '../../utils'
const chapterStudentStatusInfo=[{label: "Learning", id: 'learning'}, {label: "Assessment", id: 'assessment'}, {label: "Completed", id: 'completed'}, {label: "Need Attention", id: 'need_attention'}]
export default function ChapterWrapper(props){
    const { unit, chapter, children } = props
    return(
        <div style={{padding:'0px'}} className="chapter-wrapper">
            <Grid container style={{marginBottom: '25px'}}>
                <Grid item xs={7} className="chapter-heading">
                    <PageHeader title={chapter.title} linkInfo={{main: '/syllabus', label: 'Syllabus', sub: `UNIT - ${chapter.unit ? integer_to_roman(chapter.unit.index) : ''}:${chapter.unit ? chapter.unit.title : ''}`}}/>
                </Grid>
                <Grid item xs={5}>
                    <StudentStatus status={props.chapterStatus} statusInfo={chapterStudentStatusInfo} title="Pre-Learning status"/>
                </Grid>
            </Grid>
            {children}
        </div>
    )
}
