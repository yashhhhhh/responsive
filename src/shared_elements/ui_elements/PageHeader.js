import React from 'react'
import { Link } from 'react-router-dom';

export default function PageHeader(props){
  return(
    <div className="page-header">
      <h2>{props.title}</h2>
      {props.action}
      {props.info &&  <p>{props.info}</p>}
      {props.linkInfo ? <Link to={props.linkInfo.main}>{props.linkInfo.label} <span style={{color: '#000'}}>/</span> {props.linkInfo.sub}</Link> : null}
    </div>
  )
}
