import { combineReducers } from 'redux'
import AuthReducer from '../applications/auth/reducer'

const rootReducer = combineReducers({AuthReducer})
export default rootReducer
