const env = 'development';
const ENV_TEST = process.env.ENV_TEST
console.log('ENV_TEST', ENV_TEST)
export const appConfig = {
  api: {
    networkInterface: ({
      development: 'http://139.59.25.173:8000/',
      staging: 'https://api.upschool.tech/',
      production: 'https://api.up.school/',
    })[env],
  },
  storage:{
    storageAddress: ({
      development: 'https://upsc-sgp.sgp1.cdn.digitaloceanspaces.com/others/assets/images/',
      production: 'https://upsc-sgp.sgp1.cdn.digitaloceanspaces.com/others/assets/images/',
      staging: 'https://upsc-sgp.sgp1.cdn.digitaloceanspaces.com/others/assets/images/',
    })[env]
  }
}

export default appConfig
