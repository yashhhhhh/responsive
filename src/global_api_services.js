import axiosInstance from './utils/httpInterceptor';
export const globalGetService = (url, params) => {
  return new Promise(
    function(resolve, reject){
      axiosInstance({
        method: 'GET',
        url: url,
        params: params
      })
      .then(response => {
        resolve(response);
      })
    }
  )
}
export const globalPostService = (url, data) => {
  return new Promise(
    function(resolve, reject){
      axiosInstance({
        method: 'POST',
        url: url,
        data: data
      })
      .then(response => {
        resolve(response);
      })
    }
  )
}
export const globalPutService = (url, data) => {
  return new Promise(
      function(resolve, reject){
        axiosInstance({
          method: 'PUT',
          url: url,
          data: data
        })
        .then(response => {
          resolve(response);
        })
      }
  )
}

export const globalDownloadGetService = (url, params) => {
  return new Promise(
    function(resolve, reject){
      axiosInstance({
        method: 'GET',
        url: url,
        params: params,
        responseType: 'arraybuffer',
      })
      .then(response => {
        resolve(response);
      })
    }
  )
}

export const globalDeleteService = (url, data) => {
  return new Promise(
    function(resolve, reject){
      axiosInstance({
        method: 'DELETE',
        url: url,
        data: data
      })
      .then(response => {
        resolve(response);
      })
    }
  )
}
