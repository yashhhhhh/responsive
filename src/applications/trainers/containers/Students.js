import React , { Component } from 'react'
import StudentsList from '../components/StudentsList'
import { PageHeader } from '../../../shared_elements/ui_elements'
import { Loader, NoRecordFound } from '../../../shared_elements/components'
import { getStudentsList } from '../api_services'
import { withRouter } from 'react-router-dom'
class Students extends Component{
  constructor(props){
    super(props)
    this.state = {
      students : [],
      loader: false
    }

    this.getStudentsList = getStudentsList.bind(this)
  }

  componentDidMount(){
    this.getStudentsList(this.props.location.search)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.location.search){
      this.getStudentsList(nextProps.location.search)
    }else{
      this.getStudentsList('?')
    }
  }

  getStudentProfile = (slug) =>{
    this.props.history.push('/students/' + slug)
  }

  render(){
    const { students } = this.state
    const classInfo = JSON.parse(localStorage.getItem('syllabus'))
    return(
      <div className="students">
        <PageHeader title="Students" info={`Students profile of ${classInfo.standardName} Standard, ${classInfo.sectionName}, ${classInfo.subjectName} Subject`}/>
        {
          this.state.loader ? <Loader/> :null
        }{
          students.length ?
            students.map((student, index) =>
              <StudentsList student={student} key={index} getStudentProfile={this.getStudentProfile}/>
            )
          :!this.state.loader ? <div className="no-record-found-wapper"><NoRecordFound/></div> : null
        }
      </div>
    )
  }
}

export default withRouter(Students)
