import React,{ Component } from 'react'
import { Grid, Tabs, Tab, IconButton  } from '@material-ui/core';
import { getNeedAttenstionList, helpStudentTopic, getCloseAttenstionList, getStudentQuiz } from '../api_services'
import { TabPanel, PageHeader, SuccessButton } from '../../../shared_elements/ui_elements'
import NeedAttentionListItem from '../components/NeedAttentionListItem'
import { Loader, NoRecordFound } from '../../../shared_elements/components'
import ChapterQuizResult from '../components/ChapterQuizResult'
import { KeyboardBackspace } from '@material-ui/icons'
import CloseIcon from '@material-ui/icons/Close';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Link } from '@material-ui/core'

export default class NeedAttention extends Component{
    constructor(props){
        super(props)
        this.state = {
            activeTab: 0,
            loader: false,
            students: [],
            closedStudents: [],
            helpStudent:{
              student: {
                user:{
                  first_name: '',
                  last_name: '',
                  id: ''
                },
                chapter: {
                  id: ''
                },
                learning_type: 1
              },
              flag: false,
              type: 'edit'
            },
            quizData:{
              quiz:[],
              flag: false,
              learning_type: 1,
              title: '',
              student: {first_name: '', last_name: ''}
            },
            remarks: '',
            error:{}
        }
        this.getNeedAttenstionList = getNeedAttenstionList.bind(this)
        this.getCloseAttenstionList = getCloseAttenstionList.bind(this)
        this.helpStudentTopic = helpStudentTopic.bind(this)
        this.getStudentQuiz = getStudentQuiz.bind(this)
    }

    componentDidMount(){
      this.getNeedAttenstionList()
    }

    handleChange = (...args) =>{
        this.setState({activeTab: args[1], openTopicFlag: false, activeTopic: ''})
      }
    render(){
        const { activeTab } = this.state
        const emptyQuiz= {
          quiz:[],
          flag: false,
          learning_type: 1,
          title: '',
          student: {first_name:'', last_name:''}
        }
        return(
            <div className="need-attention">
                <PageHeader title="Need Your Attention" info="Action items that need your attention to proceed further"/>
                <Grid item xs={10}>
                    <Tabs
                            value={activeTab}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="scrollable"
                            scrollButtons="auto"
                            aria-label="scrollable auto tabs example"
                            className="need-attention-tab"
                            >
                            <Tab label={`Open (${this.state.students.length})`} id="scrollable-auto-tab-0"/>
                            <Tab label={`Closed (${this.state.closedStudents.length})`}  id="scrollable-auto-tab-1"/>
                    </Tabs>
                    {
                      !this.state.loader ?
                      <TabPanel value={activeTab} index={0}>
                              {this.state.students.map((item, index)=>{
                                 return <NeedAttentionListItem  type='edit' helpStudent={() => this.setState({helpStudent:{student: item, flag: true, type: 'edit'}})} key={index} data={item}/>
                              })}
                              {
                                !this.state.loader && !this.state.students.length ?
                                <div className="no-record-found-wapper"><NoRecordFound/></div>
                                :null
                              }
                      </TabPanel>
                      :null
                    }
                    {
                      !this.state.loader ?
                      <TabPanel value={activeTab} index={1}>
                        {this.state.closedStudents.map((item, index)=>{
                           return <NeedAttentionListItem type='' helpStudent={() => this.setState({helpStudent:{student: item, flag: true, type: ''}})} key={index} data={item}/>
                        })}
                        {
                          !this.state.closedStudents.length ?
                          <div className="no-record-found-wapper"><NoRecordFound/></div>
                          :null
                        }
                      </TabPanel>
                      :null
                    }
                </Grid>
                { this.state.loader ? <Loader/> : null}
                {
                  this.state.helpStudent.flag ?
                  <Dialog className="chapter-unlock-modal" fullWidth={true} open={this.state.helpStudent.flag} onClose={() => this.setState({helpStudent:{flag: false, type:'', student: {}, remarks: ''}})}>
                    <DialogTitle>Help {this.state.helpStudent.student.user.first_name} {this.state.helpStudent.student.user.last_name ? this.state.helpStudent.student.user.last_name : ''} in {this.state.helpStudent.student.learning_type === 1 ? 'Pre' : 'Post'}-Learning<IconButton style={{float: 'right'}} onClick={() => this.setState({helpStudent:{flag: false, type:'', student: {}, remarks: ''}})}><CloseIcon/></IconButton></DialogTitle>
                    <DialogContent className="body" style={{minHeight: '230px', boxShadow: 'inset 0px 0px 0px rgba(180, 180, 180, 0.5)'}}>
                      <DialogContentText>
                      </DialogContentText>
                      {
                        this.state.helpStudent.student.learning_type === 2 ?
                        <Link style={{cursor: 'pointer', color: '#3482FF'}} onClick={() => this.getStudentQuiz({topic: this.state.helpStudent.student.topic.id, quiz_type: this.state.helpStudent.student.quiz_type, student_id: this.state.helpStudent.student.user.id})}>View {this.state.helpStudent.student.user.first_name} {this.state.helpStudent.student.user.last_name}'s assessment details</Link>
                        :null
                      }
                        <h4>Your Feedback!</h4>
                        {
                          this.state.helpStudent.type === 'edit' ?
                            <textarea onChange={(evt) => this.setState({remarks: evt.target.value, error: {}})} value={this.state.remarks} rows={15} cols={75}></textarea>
                          :<textarea disabled rows={15} cols={75}>{this.state.helpStudent.student.remarks}</textarea>
                        }
                        <h6 className="error">{this.state.error.remarks}</h6>
                    </DialogContent>
                    {
                      this.state.helpStudent.type === 'edit' ?
                      <DialogActions className="footer">
                        <SuccessButton onClick={() => this.helpStudentTopic(this.state.helpStudent.student.chapter.id, this.state.helpStudent.student.learning_type === 1 ? {learning_type: this.state.helpStudent.student.learning_type, student_id: this.state.helpStudent.student.user.id, remarks: this.state.remarks} : {learning_type: this.state.helpStudent.student.learning_type, student_id: this.state.helpStudent.student.user.id, topic_id: this.state.helpStudent.student.topic.id , remarks: this.state.remarks})} value="Unlock"></SuccessButton>
                        <Link onClick={() => this.setState({helpStudent:{flag: false, student: {}},remarks: ''})} className="danger-link">Not Now</Link>
                      </DialogActions>
                      :null
                    }
                  </Dialog>
                  :null
                }
                <Dialog className="chapter-unlock-modal" fullScreen={true} open={this.state.quizData.flag} onClose={() => this.setState({quizData: emptyQuiz})}>
                  <DialogTitle><IconButton onClick={() => this.setState({quizData: emptyQuiz})}><KeyboardBackspace/></IconButton>{this.state.quizData.title} <span>({this.state.quizData.learning_type === 1 ? 'Pre' : 'Post'} Learning Quiz)</span><IconButton style={{float: 'right'}} onClick={() => this.setState({quizData: emptyQuiz})}><CloseIcon/></IconButton></DialogTitle>
                    <DialogContent className="body">
                    <DialogContentText className="class-test-status-table">
                      <Grid container className="class-test-status-table-header">
                                  <Grid item xs={1} size="small">Sl No.</Grid>
                                  <Grid item xs={2} >Type</Grid>
                                  <Grid item xs={7}>Question</Grid>
                                  <Grid item xs={2} style={{paddingLeft: '15px'}}>Result</Grid>
                      </Grid>
                      <Grid container>
                          {this.state.quizData.quiz && this.state.quizData.quiz.map((row,index) => {
                            return <ChapterQuizResult quizData={row} key={index} index={index}/>
                          })}
                      </Grid>
                    </DialogContentText>
                  </DialogContent>
                </Dialog>
            </div>
        )
    }
}
