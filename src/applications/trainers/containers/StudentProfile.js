import React, { Component } from 'react'
import { Link, Grid, Avatar, Tabs, Tab, CardActions,Button, } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { TabPanel, ExpansionList } from '../../../shared_elements/ui_elements'
import { NoRecordFound, Loader } from '../../../shared_elements/components'
import TimeLineList from '../components/TimeLineList'
import ChapterDetailList from '../components/ChapterDetailList'
import ClassTestList from '../components/ClassTestList'
import ChapterQuizResult from '../components/ChapterQuizResult'
import { getStudentSyllabus, getStudentActions, getStudentTest, getStudentQuiz } from '../api_services'
import { withRouter } from 'react-router-dom'
import { Dialog, DialogTitle, DialogContent, DialogContentText, IconButton } from '@material-ui/core'
import { KeyboardBackspace } from '@material-ui/icons'
import CloseIcon from '@material-ui/icons/Close';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import SyllabusProgressBar from '../components/SyllabusProgressBar'
import LearningProgressBar from '../components/LearningProgressBar'
import LinearProgress from "@material-ui/core/LinearProgress";
import { integer_to_roman } from '../../../utils'
import moment from 'moment'
const classTestHeadRow=["Test", "Status", "Date", "Attended", "Passed", "Failed"]
class StudentProfile extends Component{
  constructor(props){
    super(props)
    this.state = {
      toggleopen: true,
      activeTab: 0,
      secondTab:0,
      syllabus: [],
      student:{},
      activity: {actions: [], pagination:{}},
      classTests: [],
      quizData:{
        quiz:[],
        flag: false,
        learning_type: 1,
        title: '',
        student: {first_name: '', last_name: ''}
      },
      loader: false
    }
    this.getStudentSyllabus = getStudentSyllabus.bind(this)
    this.getStudentActions = getStudentActions.bind(this)
    this.getStudentTest = getStudentTest.bind(this)
    this.getStudentQuiz = getStudentQuiz.bind(this)
  }

  componentDidMount(){
    // this.getStudentSyllabus()
    // this.getStudentActions()
    // this.getStudentTest()
  }

  handleChange = (...args) =>{
    this.setState({activeTab: args[1]})
  }
  secondChange = (...args) =>{
    this.setState({secondTab: args[1]})
  }
  abc =()=>{
    this.props.props.history.push('/learn_view')
     console.log(this.props)
  
   }

  render(){
    const emptyQuiz= {
      quiz:[],
      flag: false,
      learning_type: 1,
      title: '',
      student: {first_name:'', last_name:''}
    }
    return(
      <div>
       {/* main div */}
        
        <Link className="primary-link" href="/students"><ArrowBack style={{verticalAlign: 'sub', marginRight: '2px'}}/>Back to Students</Link>
        <div className="student-profile" style={{marginRight:'0px'}}>
          <Grid container className="student-profile">
            <Grid item xs={7}>
              {
                this.state.student.profile_pic ?
                <Avatar className="student-image" src={this.state.student.profile_pic}></Avatar>
                :<Avatar className="student-image">{this.state.student.full_name ? this.state.student.full_name.substr(0,1) : ''}</Avatar>
              }
              <div className="student-detail">
                <h4>{this.state.student.full_name}</h4>
                <p>{this.state.student.roll_number ? this.state.student.roll_number : ''}</p>
              </div>
            </Grid>
            {/* <Grid item xs={3}>
              <p>Last Login</p>
            <span>{this.state.student.last_login ? moment(this.state.student.last_login).format('DD MMM YYYY, HH:mm a') : '--'}</span>
            </Grid>
            <Grid item xs={2}>
              <p>Avg Daily Time</p>
              <span>{this.state.student.avg_daily_usage} (Mins)</span>
            </Grid> */}
          </Grid>
          <Tabs
            value={this.state.activeTab}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
            className="student-profile-tabs"
          >
            <Tab label="Syllabus" id="scrollable-auto-tab-0"/>
            <Tab label="Class Test" id="scrollable-auto-tab-1"/>
            <Tab label="Worksheets" style={{}} id="scrollable-auto-tab-2"/>
            <Tab label="Usage" style={{display: 'none'}} id="scrollable-auto-tab-3"/>
          </Tabs>
          <TabPanel value={this.state.activeTab} index={0}>
            <div className="syllabus-container">
              
                
                  <div>
                    <h4 style={{marginBottom: '20px'}}>UNIT</h4>
                    
                      
                        <ExpansionList   toggle={this.state.toggleopen ? true : false} title={
                            // <div><p>Chapter: {"1"}</p>
                            //   <h4>{'Light'}</h4>
                            // </div>
                            <div style={{display:'flex',width:'100%', position: 'relative', zindex: '20'}} >
                            <Grid item xs={4} className="student-profile-syllabus-card" >
                            <p>Chapter: {"1"}</p>
                              <h4>{'Light'}</h4>
                              
                            </Grid>
                            
                            <Grid item xs={4} >
                              <p><span >Teaching Learning Effectivness:</span>
                              <span style={{color: 'yellow'}}> Medium</span>
                              {/* <span> <FiberManualRecordIcon style={{color: 'green'}}/></span> */}
                              </p>
                             
                            </Grid>
                            <Grid item xs={4}>
                              <p style={{marginLeft:'5px'}}><span style={{float:'left'}}>Overall Completion Status</span>
                              <span >
                              <FiberManualRecordIcon style={{color: 'green'}}/>
                              </span>
                              </p>
                             
                            </Grid>
                            </div>
                        }
                        className="chapter-list">
                          <Grid container spacing={2} className="syllabus-detail">
                            <Grid item xs={5} className="pre-learning">
                              <h4>Pre-learning</h4>
                              {/* <p>Data</p>
                              <p>commenced on:-</p>
                              <p>Due Date:-</p>
                              <p>Completion Status
          <SyllabusProgressBar status={{}} />
          <LinearProgress />
          
          </p> */}
       
          {/* <div className='res-dates' style={{display:'flex',width:'100%'}}> */}
          <div className='res-dates'>
          <Grid item xs={4}>
            <p>commenced on:-</p>
            <p>25th March 2020</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Due Date:-</span></p>
            <p>25th March 2020</p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Completion Status</span></p>
            <div style={{marginBottom:'-8px',marginTop:'5px'}}>
            <FiberManualRecordIcon style={{marginLeft:'35px',color: 'green'}}/>
            </div>
            <div>
            <p style={{color:'#32C5FF',marginTop:'0px',marginLeft:'13px'}} onClick={this.abc}>(View Status)</p>
            </div>
          </Grid>
          </div>
          {/* <CardActions>
          <Button variant="contained" size="small" className="btn" onClick={this.abc}>View</Button>
         
        
          </CardActions> */}
          
                            {/* {
                              chapter.topics.pre_learning ?
                              chapter.topics.pre_learning.map((topic, index) =>
                                <ChapterDetailList getStudentQuiz={(quiz_type) => this.getStudentQuiz({topic: topic.id, quiz_type, student_id: this.state.student.id})} topic={topic} key={index}/>
                              ):null
                            } */}
                            </Grid>
                            <Grid item xs={7} className="post-learning">
                            <h4>Post-learning</h4>
                            <Tabs
            value={this.state.secondTab}
            onChange={this.secondChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
            className="student-profile-tabs"
            style={{marginBottom:'40px'}}
          >
            <Tab label="All" id="scrollable-auto-tab-0"/>
            <Tab label="On-going" id="scrollable-auto-tab-1"/>
            <Tab label="Completed" style={{}} id="scrollable-auto-tab-2"/>
            <Tab label="Not yet started"  id="scrollable-auto-tab-3"/>
          </Tabs>
          
                              {/* {
                                chapter.topics.post_learning ?
                                chapter.topics.post_learning.map((topic, index) =>
                                  <ChapterDetailList type="post" getStudentQuiz={(quiz_type) => this.getStudentQuiz({topic: topic.id, quiz_type, student_id: this.state.student.id})} topic={topic} key={index}/>
                                ):null
                              } */}
                                           {/* <p>Syllabus Topics:-</p>
                                           <p>commenced on:-</p>
                              <p>Due Date:-</p>
                              <p>Completion Status
          <SyllabusProgressBar status={{}} />
          <LinearProgress />
          
          </p>
          <CardActions>
          <Button variant="contained" size="small" className="btn" onClick={this.abc}>View</Button>
         
        
          </CardActions> */}
      
          <TabPanel value={this.state.secondTab} index={0}>
          <h3>Refraction</h3>
              <div style={{display:'flex',width:'100%'}}>
              
          <Grid item xs={4}>
            <p>commenced on:-</p>
            <p>25th March 2020</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Due Date:-</span></p>
            <p>25th March 2020</p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Completion Status</span></p>
            {/* <div style={{marginBottom:'-8px',marginTop:'5px'}}>
            <FiberManualRecordIcon style={{marginLeft:'35px',color: 'green'}}/>
            </div> */}
            <p style={{marginLeft:'22px'}}>On-going</p>
            <div>
            <p style={{color:'#32C5FF',marginTop:'0px',marginLeft:'13px'}} onClick={this.abc}>(View Status)</p>
            </div>
          </Grid>
          </div>
          </TabPanel>
          <TabPanel value={this.state.secondTab} index={1}>
          <h3>Introduction of light</h3>
              <div style={{display:'flex',width:'100%'}}>
              
          <Grid item xs={4}>
            <p>commenced on:-</p>
            <p>25th March 2020</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Due Date:-</span></p>
            <p>25th March 2020</p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Completion Status</span></p>
            {/* <div style={{marginBottom:'-8px',marginTop:'5px'}}>
            <FiberManualRecordIcon style={{marginLeft:'35px',color: 'green'}}/>
            </div> */}
            <p style={{marginLeft:'22px'}}>On-going</p>
            <div>
            <p style={{color:'#32C5FF',marginTop:'0px',marginLeft:'13px'}} onClick={this.abc}>(View Status)</p>
            </div>
          </Grid>
          </div>
          </TabPanel>
          <TabPanel value={this.state.secondTab} index={2}>
          <h3>Reflection</h3>
              <div style={{display:'flex',width:'100%'}}>
              
          <Grid item xs={4}>
            <p>commenced on:-</p>
            <p>25th March 2020</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Due Date:-</span></p>
            <p>25th March 2020</p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Completion Status</span></p>
            {/* <div style={{marginBottom:'-8px',marginTop:'5px'}}>
            <FiberManualRecordIcon style={{marginLeft:'35px',color: 'green'}}/>
            </div> */}
            <p style={{marginLeft:'22px'}}>On-going</p>
            <div>
            <p style={{color:'#78A342',marginTop:'0px',marginLeft:'13px'}} onClick={this.abc}>(View Status)</p>
            </div>
          </Grid>
          </div>
          </TabPanel>
          <TabPanel value={this.state.secondTab} index={3}>
          <h3>Wave</h3>
              <div style={{display:'flex',width:'100%'}}>
              
          <Grid item xs={4}>
            <p>commenced on:-</p>
            <p>25th March 2020</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Due Date:-</span></p>
            <p>25th March 2020</p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Completion Status</span></p>
            {/* <div style={{marginBottom:'-8px',marginTop:'5px'}}>
            <FiberManualRecordIcon style={{marginLeft:'35px',color: 'green'}}/>
            </div> */}
            <p style={{marginLeft:'22px'}}>On-going</p>
            <div>
            <p style={{color:'#32C5FF',marginTop:'0px',marginLeft:'13px'}} onClick={this.abc}>(View Status)</p>
            </div>
          </Grid>
          </div>
          </TabPanel>
                            </Grid>
                          </Grid>
                        </ExpansionList>
                   
                  </div>
             
            </div>
          </TabPanel>
        
          <TabPanel value={this.state.activeTab} index={1}>
          <div className="syllabus-container">
              
                
              <div>
                <h4 style={{marginBottom: '20px'}}>UNIT</h4>
                
                  
                    <ExpansionList   toggle={this.state.toggleopen ? true : false} title={
                        <div><p>Chapter: {"1"}</p>
                          <h4>{'Light'}</h4>
                        </div>
                    }
                    className="chapter-list">
                      <Grid container spacing={2} className="syllabus-detail">
                        <Grid item xs={12} className="pre-learning">
                          <h4>ChapterName</h4>
           
     
      
                 
                        </Grid>
                      </Grid>
                    </ExpansionList>
               
              </div>
         
        </div>
      </TabPanel>
          <TabPanel value={this.state.activeTab} index={2}>
            Item Three
          </TabPanel>
          <TabPanel value={this.state.activeTab} index={3}>
            Item Four
          </TabPanel>
        </div>
        {/* <div className="student-activity">
          <div className="upschool-progress" style={{display: 'none'}}>
            <p>UpSchool Score</p>
            <div>
              <div className="progress">
                <div className="progress-success"></div>
              </div>
              <div>
                <span>0 out 10</span>
              </div>
            </div>
          </div> */}
          {/* <div className="recent-activity">
            <h4>Recent Activity</h4>
          <div className="hidden-scrollbar" style={{maxHeight: window.innerHeight - 120, overflow: 'auto'}}>
              <div className="timeline">
                  {
                    this.state.activity.actions.map(action =>
                        <TimeLineList action={action}/>
                    )
                  }
                </div>
            </div>
          </div> */}
        {/* </div> */}
        <Dialog className="chapter-unlock-modal" fullScreen={true} open={this.state.quizData.flag} onClose={() => this.setState({quizData: emptyQuiz})}>
          <DialogTitle><IconButton onClick={() => this.setState({quizData: emptyQuiz})}><KeyboardBackspace/></IconButton>{this.state.quizData.title} <span>({this.state.quizData.learning_type === 1 ? 'Pre' : 'Post'} Learning Quiz)</span><IconButton style={{float: 'right'}} onClick={() => this.setState({quizData: emptyQuiz})}><CloseIcon/></IconButton></DialogTitle>
            <DialogContent className="body">
            <DialogContentText className="class-test-status-table">
              <Grid container className="class-test-status-table-header">
                          <Grid item xs={1} size="small">Sl No.</Grid>
                          <Grid item xs={2} >Type</Grid>
                          <Grid item xs={7}>Question</Grid>
                          <Grid item xs={2} style={{paddingLeft: '15px'}}>Result</Grid>
              </Grid>
              <Grid container>
              
                  {this.state.quizData.quiz && this.state.quizData.quiz.map((row,index) => {
                    return <ChapterQuizResult quizData={row} key={index} index={index}/>
                  })}
              </Grid>
            </DialogContentText>
          </DialogContent>
        </Dialog>
        {
          this.state.loader ?
            <Loader/>
          :null
        }
      </div>
    )
  }
}

export default withRouter(StudentProfile)
