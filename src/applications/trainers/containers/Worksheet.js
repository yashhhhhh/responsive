import React, { Component } from 'react'
import { IconButton, Link, Grid, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, InputLabel, Select, OutlinedInput, MenuItem,  Tabs, Tab } from '@material-ui/core'
import { PageHeader, PrimaryButton, TabPanel } from '../../../shared_elements/ui_elements'
import { NoRecordFound, Loader } from '../../../shared_elements/components'
import DatePicker from "react-datepicker";
import { getTestChapters, getWorksheetList, createWorksheet, getWorkSheetDetail, downloadWorkSheet } from '../api_services'
import "react-datepicker/dist/react-datepicker.css";
import FilterAndSearchBar from '../components/FilterAndSearchBar'
import ClassTestListingTable from '../components/ClassTestListingTable'
import WorksheetListingRow from '../components/WorksheetListingRow'
import CloseIcon from '@material-ui/icons/Close';
const classTestHeadRow=["Title", 'No. Of Questions','Submission Date', "Created Date"]
export default class Worksheet extends Component{
  constructor(props){
    super(props)
    this.state = {
      createTest: false,
      testForm: {
        title: "",
      	topics_questions: [],
      	chapter: 0,
        students: []
      },
      showStudents: false,
      error:{},
      activeTab: 0,
      chapters: [],
      topics: [],
      tests:{
        classTests: [],
        pagination: {}
      },
      loader: false,
      students: [],
      selectedStudents: [],
      classTestTypes: []
    }
    this.getTestChapters = getTestChapters.bind(this)
    this.getWorksheetList = getWorksheetList.bind(this)
    this.createWorksheet = createWorksheet.bind(this)
    this.getWorkSheetDetail = getWorkSheetDetail.bind(this)
    this.downloadWorkSheet = downloadWorkSheet.bind(this)
  }

  componentDidMount(){
    this.getTestChapters({work_sheet: true})
    this.getWorksheetList()
  }

  searchTests = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    this.getWorksheetList({search: evt.target.search.value})
  }

  selectTopic = (id) =>{
    if(!this.state.testForm.topics_questions.filter(topic => topic.id === id).length){
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: [...this.state.testForm.topics_questions, {
              id: id,
              questions: []
          }
        ]
        }
      })
    }else{
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: this.state.testForm.topics_questions.filter(topic => topic.id !== id)
        }
      })
    }
  }
  updateForm = (key, value) =>{
    let error = this.state.error
    delete error[key]
    if(key === 'chapter'){
      this.state.chapters.map(chapter =>{
        if(value === chapter.id){
          this.setState({
            topics: chapter.topics,
            testForm: {...this.state.testForm, [key]: value}
          })
        }
        return chapter
      })
    }else if(key === 'test_type' && value === 1){
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value, students: this.state.students.map(item => item.id)},
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value}
      })
    }
  }

  updateQuestion = (id, value, marks) =>{
    this.setState({
      testForm: {...this.state.testForm,
        topics_questions: this.state.testForm.topics_questions.filter(item => item.id === id).length ? this.state.testForm.topics_questions.map(item => item.id === id ?
            {...item, questions: item.questions.filter((question, queIndex) => question.marks === marks).length ? item.questions.map((question, queIndex) => question.marks === marks ? {...question, no_of_questions: value} : question) : [...item.questions, {marks, no_of_questions: value}]}
          :item): [...this.state.testForm.topics_questions, {id, questions: [{no_of_questions: value, marks}]}]
      }
    })
  }

  confirmStudents = () =>{
    this.setState({
      testForm: {...this.state.testForm, students: this.state.selectedStudents},
      showStudents: false
    })
  }

  selectStudent = (id) => {
    this.setState({
      selectedStudents: this.state.selectedStudents.includes(id) ? this.state.selectedStudents.filter(item => item !== id) : [...this.state.selectedStudents, id]
    })
  }

  selectAll = (flag) => {
    if(flag){
      this.setState({
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        selectedStudents: []
      })
    }
  }

  clearTestForm = (flag) =>{
    this.setState({
      createTest: flag,
      testForm: {
        title: "",
      	topics_questions: [],
      	chapter: 0,
        students: this.state.students.map(student => student.id),
      }
    })
  }
  handleChange = (event, newValue) => {
    if(newValue){
      this.getClassTestList({status: newValue})
    }else{
      this.getClassTestList()
    }
    this.setState({
        activeTab: newValue
    })
  }
  render(){
    const { activeTab, testForm, tests } = this.state
    var submitEnabled = false
    if(testForm.title.trim() !== '' && testForm.chapter !== 0 && Object.keys(testForm.topics_questions).length){
      submitEnabled = true
    }
    return(
      <div className="class_test_listing">
         <PageHeader
          title="Worksheets"
          info="Create worksheets on the completed chapters"
          action={tests.classTests.length && !this.state.loader ? <PrimaryButton style={{float: 'right'}} onClick={() => this.clearTestForm(true)} value="CREATE WORKSHEET" type="button"/> : null}
          />
          {
          <div className="classTest-listing-wrapper">
              <Tabs
                    value={activeTab}
                    onChange={this.handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                    className="classTest-listing-tab"
                    >
                    <Tab label="All" id="scrollable-auto-tab-0"/>
                </Tabs>
                <TabPanel value={activeTab} index={0} className="tab-panel">
                    {

                      <FilterAndSearchBar searchName="search" search={this.searchTests} placeholder="Search for worksheet.."/>

                    }
                    {
                      !this.state.loader ?
                      !tests.classTests.length ?
                      <NoRecordFound
                        title="No Worksheet Found!"
                        content="You can create worksheets on completed chapters"
                        component={<PrimaryButton onClick={() => this.setState({createTest: true})} value="CREATE WORKSHEET" type="button"/>}
                      />
                    : <ClassTestListingTable
                      headRow={classTestHeadRow}
                      tableData={
                          this.state.tests.classTests.map(test =>
                            <WorksheetListingRow downloadSheet={() => this.downloadWorkSheet(test.id)} getWorkSheetDetail={() => this.getWorkSheetDetail(test.id)} row={test} />
                          )
                      }
                    />:null
                    }
                </TabPanel>

            </div>
        }
        {
          this.state.loader ?
            <Loader/>
          :null
        }
      <Dialog className="chapter-unlock-modal class-test-crud-modal" fullWidth={true} maxWidth="md" open={this.state.createTest} onClose={() => this.setState({createTest: false})}>
          <DialogTitle>Create Worksheet<IconButton style={{float: 'right'}} onClick={() => this.setState({createTest: false})}><CloseIcon/></IconButton></DialogTitle>
            <DialogContent className="body">
            <DialogContentText>
              <div className="create-test-form">
                <div className="select-class-container" style={{width: '49%', display: 'inline-block'}}>
                  <InputLabel htmlFor="username">Title<span className="required-asterisk">*</span></InputLabel>
                  <input
                      id="title"
                      className="title"
                      defaultValue=""
                      placeholder="Enter Worksheet Title"
                      onChange={(evt) => this.updateForm('title', evt.target.value)}
                    />
                  <h6 className="error">{this.state.error.username}</h6>
                </div>
                <div style={{marginLeft: '2%', width: '49%', display: 'inline-block'}}>
                  <InputLabel htmlFor="username">Completed Chapter<span className="required-asterisk">*</span></InputLabel>
                  {
                    !this.state.chapters.length ?
                      <p>No Chapters Completed Yet!</p>
                    :<Select
                      value={this.state.testForm.chapter}
                      onChange={(evt) => this.updateForm('chapter', evt.target.value)}
                      input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                      className="chapter-select"
                    >
                    {
                      this.state.chapters.map((chapter, index) =>
                        <MenuItem key={index} value={chapter.id}>{chapter.title}</MenuItem>
                      )
                    }
                    </Select>
                  }
                </div>
              </div>
            </DialogContentText>
            {
              testForm.chapter !== '' || testForm.chapter !== 0 ?
              <div>
                <Grid container spacing={2} className="class-test-crud-topics">
                  <Grid item xs={6}><h5>Select the topics</h5></Grid>
                  <Grid item xs={6}><h5>No. Of Questions</h5></Grid>
                  {
                    this.state.topics.map((item, topicIndex) =>
                      <React.Fragment>
                        <Grid style={{cursor: 'pointer'}} item xs={6} onClick={(evt) => this.selectTopic(item.id)}>
                          <input type="checkbox" checked={this.state.testForm.topics_questions.filter(topic => topic.id === item.id).length}/>
                          <span>{item.title}</span>
                        </Grid>
                        <Grid item xs={6}>
                          <Grid container>
                          {
                            this.state.testForm.topics_questions.filter(topic => topic.id === item.id).length ?
                            Object.keys(item.marks).map(mark =>
                              <Grid item xs={2}>
                                <h5 style={{marginTop: '0px', marginBottom: '5px'}}>{mark} {mark > 1 ? 'Marks' : 'Mark'}</h5>
                                <input min={0} onChange={(evt) => this.updateQuestion(item.id, parseInt(evt.target.value), mark)} type="number"/>
                              </Grid>
                            )
                            :null
                          }
                          </Grid>
                        </Grid>

                      </React.Fragment>
                    )
                  }
                  <Grid item xs={6}>
                    <div className="select-class-container">
                      <h5>Submission Date</h5>
                        <DatePicker
                          selected={this.state.testForm.submit_date}
                          onChange={(date) => this.updateForm('submit_date', date)}
                          dateFormat="dd-MMM-yyyy"
                          minDate={new Date()}
                          className="up-datepicker"
                          popperPlacement="top-start"
                        />
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                  </Grid>
                </Grid>
              </div>:null
            }
          </DialogContent>
          <DialogActions className="footer">
            <Link onClick={() => this.clearTestForm(false)} className="primary-link">Close</Link>
            <PrimaryButton disabled={!submitEnabled} onClick={() => {this.clearTestForm(false);this.createWorksheet(testForm)}} value="SUBMIT" type="button"/>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
