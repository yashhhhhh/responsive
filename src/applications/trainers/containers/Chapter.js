import React, { Component } from 'react'
import ChapterWrapper from '../../../shared_elements/ui_elements/ChapterWrapper'
import { Link, Grid, Tabs, Tab, IconButton, } from '@material-ui/core'
import { TabPanel, SuccessButton } from '../../../shared_elements/ui_elements'
import { Loader, NoRecordFound } from '../../../shared_elements/components'
import ChapterSummaryTable from '../components/ChapterSummaryTable'
import TopicStatus from '../components/TopicStatus'
import TopicSelect from '../components/TopicSelect'
import ChapterQuizResult from '../components/ChapterQuizResult'
import DigiCard from '../components/DigiCard'
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core'
import { getTopicsList, getTopicDetails, enableTopic, getDigicard, getCardDetail, getStudentQuiz, getStudentstatuses, helpStudentTopic } from '../api_services'
import { withRouter } from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { KeyboardBackspace } from '@material-ui/icons'
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment'
import MathJax from 'react-mathjax-preview-ds'
class Chapter extends Component{
    constructor(props){
        super(props)
        this.state = {
          activeTab: 0,
          openTopicFlag: false,
          activeTopic: '',
          chapterDetail: {},
          topicStudents: {},
          loader: false,
          chapterUnlock: false,
          chapter: {},
          postLearningDate: new Date(),
          viewCard: false,
          cardList: [],
          cardDetail: {
            flag: false,
            detail: ''
          },
          helpStudent:{
            student: {},
            flag: false
          },
          chapterStatus:{
            assessment: 0,
            completed: 0,
            learning: 0,
            need_attention: 0
          },
          remarks: '',
          error:{},
          quizData:{
            quiz:[],
            flag: false,
            learning_type: 1,
            title: '',
            student: {first_name: '', last_name: ''}
          }
        }
        this.getTopicsList = getTopicsList.bind(this)
        this.getTopicDetails = getTopicDetails.bind(this)
        this.enableTopic = enableTopic.bind(this)
        this.getDigicard = getDigicard.bind(this)
        this.getCardDetail = getCardDetail.bind(this)
        this.getStudentstatuses = getStudentstatuses.bind(this)
        this.helpStudentTopic = helpStudentTopic.bind(this)
        this.getStudentQuiz = getStudentQuiz.bind(this)
      }
      componentDidMount(){
        this.getTopicsList()
        this.getStudentstatuses(this.props.match.params.chapterSlug, 1)
      }
      handleChange = (...args) =>{
        this.setState({activeTab: args[1], openTopicFlag: false, activeTopic: '', viewCard: false})
      }
      handleTopicOpen = (value) =>{
          this.setState((prevState)=> ({
              ...prevState,
              openTopicFlag: !prevState.openTopicFlag,
              activeTopic: value
          }))
          if(value){
            this.getTopicDetails(this.props.match.params.chapterSlug, value)
          }else{
            this.setState({viewCard: false})
          }
      }

      helpStudent = () =>{
        this.helpStudentTopic(this.props.match.params.chapterSlug, {student_id: this.state.helpStudent.student.id, learning_type: 2, topic_id: this.state.activeTopic, remarks: this.state.remarks})
      }

      handleTopicChange = (e) => {
          this.setState((prevState)=>({
              ...prevState,
              activeTopic: e.target.value,
          }))
          if(this.state.viewCard){
            this.getDigicard(e.target.value)
          }
          this.getTopicDetails(this.props.match.params.chapterSlug, e.target.value)
      }
      handleToggleModal = (e) => {
          this.setState((prevState)=>({
              needHelpModal: !prevState.needHelpModal
          }))
      }
    render(){
        const { openTopicFlag, activeTopic, chapterDetail } = this.state
        const emptyQuiz= {
          quiz:[],
          flag: false,
          learning_type: 1,
          title: '',
          student: {first_name:'', last_name:''}
        }

        return(
            <ChapterWrapper chapterStatus={this.state.chapterStatus} chapter={this.state.chapterDetail} unit={this.state.chapterDetail.state}>
                <div className="chapter-tabs-wrapper">
                    <Tabs
                        value={this.state.activeTab}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                        className="chapter-page-tab"
                        >
                        <Tab label="Pre-Learning" id="scrollable-auto-tab-0"/>
                        <Tab label="Post Learning" style={chapterDetail.topics && chapterDetail.topics.post_learning && chapterDetail.topics.post_learning.length ? {} : {display: 'none'}}  id="scrollable-auto-tab-1"/>
                        <Tab style={{display: 'none'}} label="Worksheet" id="scrollable-auto-tab-2"/>
                    </Tabs>
                    <TabPanel value={this.state.activeTab} index={0}>
                        <div className="pre-learning-tab" style={{paddingBottom: openTopicFlag ? '20px': '40px'}}>
                            {
                              !this.state.loader ?
                              openTopicFlag ? <TopicSelect activeTab={this.state.activeTab} getDigicard={this.getDigicard} value={activeTopic} handleTopicChange={this.handleTopicChange} chapter={{id: this.props.match.params.chapterSlug, title: this.state.chapterDetail.title, topics: chapterDetail.topics.pre_learning}} handleTopicOpen={this.handleTopicOpen}/>
                            : chapterDetail.topics && chapterDetail.topics.pre_learning && chapterDetail.topics.pre_learning.length === 0 ?
                              <NoRecordFound/>
                            :<Grid container>
                                        <Grid item xs={12} className="pre-learning-notice">
                                            <p>{`Pre-learning is active and students are preparing for the chapter. Find the students' status as below`}</p>
                                        </Grid>
                                            <ChapterSummaryTable getDigicard={this.getDigicard} type="pre" tableHead={`Pre-learning Topics`} chapterTopics={chapterDetail.topics ? chapterDetail.topics.pre_learning : []} needAttentionCount={chapterDetail.pre_learning_need_attention} handleTopicOpen={this.handleTopicOpen}/>
                                  </Grid>
                                :null
                           }
                        </div>
                    </TabPanel>
                    <TabPanel value={this.state.activeTab} index={1}>
                        <div className="post-learning-tab" style={{paddingBottom: openTopicFlag ? '20px': '40px'}}>
                            {
                              !this.state.loader ?
                              openTopicFlag ?<TopicSelect activeTab={this.state.activeTab} value={activeTopic} handleTopicChange={this.handleTopicChange} chapter={{id: this.props.match.params.chapterSlug, title: this.state.chapterDetail.title, topics: chapterDetail.topics.post_learning}} handleTopicOpen={this.handleTopicOpen}/>
                                :chapterDetail.topics && chapterDetail.topics.post_learning && !chapterDetail.topics.post_learning.length ?
                                  <NoRecordFound/>
                                :<ChapterSummaryTable getDigicard={this.getDigicard} type="post" tableHead={`Syllabus Topics`} chapterTopics={chapterDetail.topics ? chapterDetail.topics.post_learning : []} needAttentionCount={chapterDetail.pre_learning_need_attention} handleTopicOpen={this.handleTopicOpen} handleTopicModal={(topic) => this.setState({chapter: topic, chapterUnlock: true})}/>
                              :null
                            }
                        </div>
                    </TabPanel>
                    <TabPanel value={this.state.activeTab} index={2}>
                        <div className="pre-learning-tab">Worksheet Section</div>
                    </TabPanel>
                </div>
                {openTopicFlag && !this.state.loader && !this.state.viewCard ? <TopicStatus getStudentQuiz={(student_id, quiz_type) => this.getStudentQuiz({topic: this.state.activeTopic, quiz_type, student_id})} activeTab={this.state.activeTab} helpStudent={(student) => this.setState({helpStudent:{flag: true, student: student}})} topicStudents={this.state.topicStudents}/> : null}
                {
                  this.state.viewCard ?
                  <div>
                    <Grid container spacing={2} className="digicards-tab">
                      {
                        this.state.cardList.length ?
                          this.state.cardList.map((card, index) =>
                            <DigiCard readMore={() => this.getCardDetail(this.state.activeTopic, card.id)} card={card} key={'card'+index}/>
                          )
                        :null
                      }
                    </Grid>
                  </div>
                  :null
                }
                {
                  this.state.loader ? <Loader/> :null
                }
                <Dialog className="chapter-unlock-modal" fullWidth={true} open={this.state.chapterUnlock} onClose={() => this.setState({chapterUnlock: false})}>
                  <DialogTitle>Unlock the Topic<IconButton style={{float: 'right'}} onClick={() => this.setState({chapterUnlock: false})}><CloseIcon/></IconButton></DialogTitle>
                    <DialogContent className="body">
                    <DialogContentText>
                      You want to unlock the topic <strong>{`"${this.state.chapter.title}"`}</strong><br/>
                    of <strong>{`${this.state.chapterDetail.title}`}</strong>
                    </DialogContentText>
                    <p className="notice">Your students will get notified and can access post-learning for the chapter</p>
                      <p style={{fontSize: '12px'}}>
                        Last date to complete post-learning:
                        <DatePicker
                          selected={this.state.postLearningDate}
                          onChange={(date) => this.setState({postLearningDate: date})}
                          dateFormat="dd-MMM-yyyy"
                          minDate={new Date()}
                          className="up-datepicker"
                        />
                      </p>
                  </DialogContent>
                  <DialogActions className="footer">
                    <SuccessButton onClick={() => this.enableTopic(this.state.chapter.id, JSON.parse(localStorage.getItem('syllabus')).section, moment(this.state.postLearningDate).format('YYYY-MM-DD'))} value="Yes"></SuccessButton>
                    <Link onClick={() => this.setState({chapterUnlock: false})} className="danger-link">Not Now</Link>
                  </DialogActions>
                </Dialog>
                <Dialog className="chapter-unlock-modal" fullWidth={true} open={this.state.helpStudent.flag} onClose={() => this.setState({helpStudent:{flag: false, student: {}}})}>
                  <DialogTitle>Help {this.state.helpStudent.student.first_name} {this.state.helpStudent.student.last_name} in Post-Learning<IconButton style={{float: 'right'}} onClick={() => this.setState({helpStudent:{flag: false, student: {}}})}><CloseIcon/></IconButton></DialogTitle>
                    <DialogContent className="body" style={{minHeight: '230px', boxShadow: 'inset 0px 0px 0px rgba(180, 180, 180, 0.5)'}}>
                    <DialogContentText>
                    </DialogContentText>
                    <Link style={{cursor: 'pointer', color: '#3482FF'}} onClick={() => this.getStudentQuiz({topic: this.state.activeTopic, quiz_type: this.state.helpStudent.student.quiz_type, student_id: this.state.helpStudent.student.id})}>View {this.state.helpStudent.student.first_name} {this.state.helpStudent.student.last_name}'s assessment details</Link>
                      <h4>Your Feedback!</h4>
                    <textarea onChange={(evt) => this.setState({remarks: evt.target.value, error: {}})} value={this.state.remarks} rows={15} cols={75}></textarea>
                        <h6 className="error">{this.state.error.remarks}</h6>
                  </DialogContent>
                  <DialogActions className="footer">
                    <SuccessButton onClick={() => this.helpStudent()} value="Unlock"></SuccessButton>
                    <Link onClick={() => this.setState({helpStudent:{flag: false, student: {first_name: '', last_name: ''}}})} className="danger-link">Not Now</Link>
                  </DialogActions>
                </Dialog>
                <Dialog className="chapter-unlock-modal" fullScreen={true} open={this.state.cardDetail.flag} onClose={() => this.setState({cardDetail:{detail: '', flag: false}})}>
                  <DialogTitle><IconButton onClick={() => this.setState({cardDetail:{detail: '', flag: false}})}><KeyboardBackspace/></IconButton>{this.state.cardDetail.detail.title}<IconButton style={{float: 'right'}} onClick={() => this.setState({cardDetail:{detail: '', flag: false}})}><CloseIcon/></IconButton></DialogTitle>
                    <DialogContent className="body" style={{boxShadow: "inset 0 0px 0px hsla(0,0%,70.6%,.5)"}}>
                    <DialogContentText>
                      <iframe style={{width: '100%', height: window.innerHeight -100, borderWidth: '0px'}} srcdoc={this.state.cardDetail.detail.description}></iframe>
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions className="footer">
                  </DialogActions>
                </Dialog>
                <Dialog className="chapter-unlock-modal" fullScreen={true} open={this.state.quizData.flag} onClose={() => this.setState({quizData: emptyQuiz})}>
                  <DialogTitle><IconButton onClick={() => this.setState({quizData: emptyQuiz})}><KeyboardBackspace/></IconButton>{this.state.quizData.title} <span>({this.state.quizData.learning_type === 1 ? 'Pre' : 'Post'} Learning Quiz)</span><IconButton style={{float: 'right'}} onClick={() => this.setState({quizData: emptyQuiz})}><CloseIcon/></IconButton></DialogTitle>
                <DialogContent className="body" style={{boxShadow: 'none'}}>
                    <DialogContentText className="class-test-status-table">
                      <Grid container className="class-test-status-table-header">
                                  <Grid item xs={1} size="small">Sl No.</Grid>
                                  <Grid item xs={2} >Type</Grid>
                                  <Grid item xs={7}>Question</Grid>
                                  <Grid item xs={2} style={{paddingLeft: '15px'}}>Result</Grid>
                      </Grid>
                      <Grid container>
                          {this.state.quizData.quiz && this.state.quizData.quiz.map((row,index) => {
                            return <ChapterQuizResult quizData={row} key={index} index={index}/>
                          })}
                      </Grid>
                    </DialogContentText>
                  </DialogContent>
                </Dialog>
            </ChapterWrapper>
        )
    }
}
export default withRouter(Chapter)
