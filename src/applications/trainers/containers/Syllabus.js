import React from 'react'
import { PageHeader, SuccessButton } from '../../../shared_elements/ui_elements'
import { Loader } from '../../../shared_elements/components'
import { Grid, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Link, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import ChapterCard from '../components/ChapterCard'
import { globalGetService } from '../../../global_api_services'
import { enableChapter, getSyllabus } from '../api_services'
import { withRouter } from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { integer_to_roman } from '../../../utils'
import moment from 'moment'
import MathJax from 'react-mathjax-preview-ds'
class Syllabus extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      syllabus: [],
      chapterUnlock: false,
      chapter: {},
      unit: {},
      loader: false,
      postLearningDate: new Date()
    }

    this.enableChapter = enableChapter.bind(this)
    this.getSyllabus = getSyllabus.bind(this)
  }

  componentDidMount(){
    this.getSyllabus()
  }

  render(){
    return(
      <div>
       
        <PageHeader title="Syllabus" info={`Complete syllabus of ${JSON.parse(localStorage.getItem('syllabus')).subjectName} with status`}/>
        {
          this.state.syllabus && this.state.syllabus.length ?
          this.state.syllabus.map((unit, unitIndex) =>
          <Grid container spacing={2} key={unitIndex} className="syllabus-container">
            <Grid item xs={12} style={{paddingBottom: '0px'}}>
              <h1 className="unit-heading">UNIT  - {integer_to_roman(unit.index)} : {unit.title}</h1>
              
            </Grid>
            {
              unit.chapters.map((chapter, chapterIndex) =>
              <ChapterCard viewChapter={() => this.props.history.push({pathname:`/syllabus/chapter/${chapter.id}`, state: unit})} chapter={chapter} unlockChapter={() => this.setState({ chapter: chapter,unit: unit, chapterUnlock: true, postLearningDate: new Date()})} key={chapterIndex}/>
              )
            }
          </Grid>
          )
          :null
        }
        <Dialog className="chapter-unlock-modal" maxWidth="md" open={this.state.chapterUnlock} onClose={() => this.setState({chapterUnlock: false})}>
          <DialogTitle>Unlock the Chapter<IconButton style={{float: 'right'}} onClick={() => this.setState({chapterUnlock: false})}><CloseIcon/></IconButton></DialogTitle>
            <DialogContent className="body">
            <DialogContentText>
              You want to unlock the chapter <strong>{`"${this.state.chapter.title}"`}</strong><br/>
            of <strong>UNIT - {`${integer_to_roman(this.state.unit.index)}: ${this.state.unit.title}`}</strong>
            </DialogContentText>
            <p className="notice">Your students will get notified and can access pre-learning for the chapter</p>
            <p style={{fontSize: '12px'}}>
              Expected date of unlocking post-learning:
              <DatePicker
                selected={this.state.postLearningDate}
                onChange={(date) => this.setState({postLearningDate: date})}
                dateFormat="dd-MMM-yyyy"
                minDate={new Date()}
                className="up-datepicker"
              />
            </p>
          </DialogContent>
          <DialogActions className="footer">
            <SuccessButton onClick={() => this.enableChapter(this.state.chapter.id, JSON.parse(localStorage.getItem('syllabus')).section, moment(this.state.postLearningDate).format('YYYY-MM-DD'))} value="Yes"></SuccessButton>
            <Link onClick={() => this.setState({chapterUnlock: false})} className="danger-link">Not Now</Link>
          </DialogActions>
        </Dialog>
        {
          this.state.loader ? <Loader/> :null
        }
      </div>
    )
  }
}

export default withRouter(Syllabus)
