import React,{Fragment} from 'react'
import { Tabs, Tab } from '@material-ui/core'
import { TabPanel, ClassTestWrapper } from '../../../shared_elements/ui_elements'
import StatusTable from '../components/StatusTable'
import { withRouter } from 'react-router-dom'
import FilterAndSearchBar from '../components/FilterAndSearchBar'
import ClassTestListingTable from '../components/ClassTestListingTable'
import ClassTestDetailRow from '../components/ClassTestDetailRow'
import SelectStudent from '../components/SelectStudent'
import ClassTestPrimaryDetails from '../components/ClassTestPrimaryDetails'
import { getClassTestDetail, getClassTestStudents, getStudentTestDetail } from '../api_services'
import { Loader, NoRecordFound } from '../../../shared_elements/components'
const classTestDetailHeadRow=["Student Name", "Attempted", "Right", "Wrong", "Result"]
class ClassTestDetail extends React.Component{
      constructor(props){
          super(props)
          this.state = {
              loader: false,
              activeTab: 0,
              openStudentFlag: false,
              activeStudent: '',
              testDetail: {
                datetime: null,
                id: 0,
                no_of_questions: 0,
                pass_mark: 0,
                title: "",
                topics: [],
                total_time: 0,
                chapter:{
                  title: '',
                  index: 0
                }
              },
              students: {
                students:[],
                pagination: {}
              },
              quizData:{
                quiz:[],
                title: '',
                student: {first_name: '', last_name: ''}
              }
          }
          this.getClassTestDetail = getClassTestDetail.bind(this)
          this.getClassTestStudents = getClassTestStudents.bind(this)
          this.getStudentTestDetail = getStudentTestDetail.bind(this)
      }

      componentDidMount(){
        this.getClassTestDetail()
      }

      handleChange = (event, newValue) => {
          this.setState({
              activeTab: newValue,
              openStudentFlag: false
          })
      }
      handleStudentOpen = (value) => {
        this.setState((prevState)=> ({
            ...prevState,
            activeStudent: value
        }))
        if(value){
          this.getStudentTestDetail(this.props.match.params.classTestSlug, value)
        }else{
          this.setState({
            openStudentFlag: false
          })
        }
      }
      handleStudentChange = (e) => {
          this.setState((prevState)=>({
              ...prevState,
              activeStudent: e.target.value
          }))
          this.getStudentTestDetail(this.props.match.params.classTestSlug, e.target.value)
      }
      render(){
          const { activeTab, openStudentFlag, loader, testDetail, students } = this.state
        return(
            <ClassTestWrapper classTest={testDetail} classTestStatus={{attended: testDetail.attended, passed: testDetail.passed, failed: testDetail.failed}}>
                <ClassTestPrimaryDetails
                        detail={testDetail}
                        topics={testDetail.topics}
                    />
                <div className="classTest-detail-tab-wrapper">
                    <Tabs
                        value={activeTab}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                        className="classTest-detail-tabs"
                        >
                        <Tab label={`Failed(${students.students.filter(item => item.status === 2).length })`} id="scrollable-auto-tab-0"/>
                        <Tab label={`Passed(${students.students.filter(item => item.status === 1).length })`}  id="scrollable-auto-tab-1"/>
                        <Tab label={`Not Attended(${students.students.filter(item => item.status === 0).length })`}  id="scrollable-auto-tab-3"/>
                        <Tab label={`Unable to Attempt(${students.students.filter(item => item.status === 4).length })`}  id="scrollable-auto-tab-4"/>
                    </Tabs>
                    <TabPanel value={activeTab} index={0} className="tab-panel">

                        {openStudentFlag ? <SelectStudent handleStudentOpen={this.handleStudentOpen} value={this.state.activeStudent} handleStudentChange={this.handleStudentChange} students={this.state.students.students}/> : <Fragment>{students.students.length ? <FilterAndSearchBar style={{display: 'none'}} placeholder="Search for test.."/> : null}
                      {
                        students.students.length && students.students.filter(item => item.status === 2).length ?
                        <ClassTestListingTable
                            headRow={classTestDetailHeadRow}
                            tableData={
                              this.state.students.students.map(student => student.status === 2 ?
                                <ClassTestDetailRow row={{...student, result: `Failed (${student.right}/${student.total})`}} handleStudentOpen={this.handleStudentOpen} />:null)
                            }
                        />
                      :!loader ? <NoRecordFound/> : null
                      }
                        </Fragment>}
                    </TabPanel>
                    <TabPanel value={activeTab} index={1} className="tab-panel">
                      {openStudentFlag ? <SelectStudent handleStudentOpen={this.handleStudentOpen} value={this.state.activeStudent} handleStudentChange={this.handleStudentChange} students={this.state.students.students.filter(student => student.status === 1)}/> : <Fragment>{students.students.length ? <FilterAndSearchBar style={{display: 'none'}} placeholder="Search for test.."/> : null}
                    {
                      students.students.length && students.students.filter(item => item.status === 1).length ?
                      <ClassTestListingTable
                          headRow={classTestDetailHeadRow}
                          tableData={
                            this.state.students.students.map(student => student.status === 1 ?
                              <ClassTestDetailRow row={{...student, result: `Passed (${student.right}/${student.total})`}} handleStudentOpen={this.handleStudentOpen} />:null)
                          }
                      />
                    :!loader ? <NoRecordFound/> : null
                    }
                      </Fragment>}
                    </TabPanel>
                    <TabPanel value={activeTab} index={2} className="tab-panel">

                        {openStudentFlag ? <SelectStudent handleStudentOpen={this.handleStudentOpen} value={this.state.activeStudent} handleStudentChange={this.handleStudentChange} students={this.state.students.students}/> : <Fragment>{students.students.length ? <FilterAndSearchBar style={{display: 'none'}} placeholder="Search for test.."/> : null}
                      {
                        students.students.length && students.students.filter(item => item.status === 0).length ?
                        <ClassTestListingTable
                            headRow={classTestDetailHeadRow}
                            tableData={
                              this.state.students.students.map(student => student.status === 0 ?
                                <ClassTestDetailRow row={{...student, result: `Not Attended`}} handleStudentOpen={this.handleStudentOpen} />:null)
                            }
                        />
                      :!loader ? <NoRecordFound/> : null
                      }
                        </Fragment>}
                    </TabPanel>
                    <TabPanel value={activeTab} index={3} className="tab-panel">

                        {openStudentFlag ? <SelectStudent handleStudentOpen={this.handleStudentOpen} value={this.state.activeStudent} handleStudentChange={this.handleStudentChange} students={this.state.students.students}/> : <Fragment>{students.students.length ? <FilterAndSearchBar style={{display: 'none'}} placeholder="Search for test.."/> : null}
                      {
                        students.students.length && students.students.filter(item => item.status === 4).length ?
                        <ClassTestListingTable
                            headRow={classTestDetailHeadRow}
                            tableData={
                              this.state.students.students.map(student => student.status === 4 ?
                                <ClassTestDetailRow row={{...student, result: `Not Attended`}} handleStudentOpen={this.handleStudentOpen} />:null)
                            }
                        />
                      :!loader ? <NoRecordFound/> : null
                      }
                        </Fragment>}
                    </TabPanel>
                </div>
                {this.state.openStudentFlag && !this.state.loader ? <StatusTable quizData={this.state.quizData} rows={[]}/> : null}
                {this.state.loader ? <Loader/> : null}
            </ClassTestWrapper>
        )
    }
}

export default withRouter(ClassTestDetail)
