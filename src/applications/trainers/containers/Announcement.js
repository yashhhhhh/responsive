import React,{ Component } from 'react'
import { Grid, InputLabel, TextField } from '@material-ui/core';
import { PageHeader, PrimaryButton } from '../../../shared_elements/ui_elements'
import AnnouncementCard from '../components/AnnouncementCard'
const announcementData = {publishDate: '17th July 2019', content: 'Please come pre-pared for workshop at class on Electricity Chapter 12 Tomorrow', status:{viewed: '23', notViewed: '2'}}
export default class Announcement extends Component{
    constructor(props){
        super(props)
        this.state = {
            announcement: '',
            error: {},
            loader: false
        }
    }
    handleChange = (key, value) => {
       this.setState((prevState)=>({
           ...prevState,
           [key]: value,
           error: {
               [key]: ''
           }
       }))
    }
    handleSubmit = () => {
        const {announcement} = this.state
        this.setState({loader: true})
        if(announcement === ''){
            this.setState((prevState)=> ({
                ...prevState,
                error: {
                    announcement: 'Please enter any update to students'
                },
                loader: false
            }))
        }else{
            console.log('Submit clicked ', announcement)
        }
    }
    render(){
        const { announcement, error, loader } = this.state
        return(
            <Grid container className="announcement">
                    <PageHeader title="Announcement" info="Share update with entire 9th Standard, A-Section, Physics Students"/>
                    <Grid item xs={7} className="text-wrapper">
                        <InputLabel htmlFor="announcement">Announcement</InputLabel>
                        <TextField
                            id="announcement"
                            value={announcement}
                            onChange={(e)=>{this.handleChange('announcement', e.target.value)}}
                            margin="normal"
                            variant="outlined"
                            placeholder="Enter your update to students"
                            multiline={true}
                            rows={3}
                            rowsMax={4}
                            fullWidth={true}
                        />
                        <h6 className="error">{error.announcement ? error.announcement : ''}</h6>
                        <PrimaryButton type="submit" disabled={loader} value={loader ? "Processing..":"Submit"} onClick={this.handleSubmit}></PrimaryButton>
                    </Grid>
                    <Grid item xs={11}>
                        <AnnouncementCard data={announcementData}/>
                    </Grid>
            </Grid>
        )
    }
}
