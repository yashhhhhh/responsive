import React, { Component } from 'react'
import { IconButton, Link, Grid, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, InputLabel, Select, OutlinedInput, MenuItem,  Tabs, Tab } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import { PageHeader, PrimaryButton, TabPanel } from '../../../shared_elements/ui_elements'
import { NoRecordFound, Loader } from '../../../shared_elements/components'
import DatePicker from "react-datepicker";
import { getTestChapters, getClassTestList, createClassTest, getConstants, deleteClassTest } from '../api_services'
import "react-datepicker/dist/react-datepicker.css";
import FilterAndSearchBar from '../components/FilterAndSearchBar'
import ClassTestListingTable from '../components/ClassTestListingTable'
import ClassTestListingRow from '../components/ClassTestListingRow'
import moment from 'moment'

const classTestHeadRow=["pre", "Status", "Date", "Attended", "Passed", "Failed"]
export default class Prelearnview extends Component{
  constructor(props){
    super(props)
    this.state = {
      createTest: false,
      testForm: {
        title: "",
        mode:'add',
      	topics_questions: {
      	},
      	chapter: 0,
      	duration: 10,
      	min_correct_ans: 1,
      	scheduled_date_time: "",
        schedule: false,
        students: [],
        test_type: 1
      },
      showStudents: false,
      activeTab: 0,
      chapters: [],
      topics: [],
      tests:{
        classTests: [],
        pagination: {
          total: 0,
          current_page: 1
        }
      },
      error:{},
      loader: false,
      students: [],
      selectedStudents: [],
      classTestTypes: [],
      deleteTest: {
        title: '',
        flag: false
      }
    }
    this.getTestChapters = getTestChapters.bind(this)
    this.getClassTestList = getClassTestList.bind(this)
    this.createClassTest = createClassTest.bind(this)
    this.deleteClassTest = deleteClassTest.bind(this)
    this.getConstants = getConstants.bind(this)
  }

  componentDidMount(){
    this.getTestChapters()
    this.getClassTestList()
    this.getConstants('class_test_type')
  }

  editTest = (test) =>{
    this.setState({testForm: {...test, mode: 'edit', schedule: true, scheduled_date_time: new Date(test.scheduled_date_time)}, createTest: true})
    this.state.chapters.map(chapter =>{
      if(test.chapter === chapter.id){
        this.setState({
          topics: chapter.topics,
        })
      }
      return chapter
    })
  }

  deleteTest = (test) =>{
    this.setState({deleteTest: {title: test.title, flag: true, id: test.id}})
  }

  selectTopic = (id) =>{
    if(!this.state.testForm.topics_questions[id]){
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: {...this.state.testForm.topics_questions, [id]: {no_of_objective_questions: 0, no_of_subjective_questions: 0}}
        }
      })
    }else{
      let topics_questions = this.state.testForm.topics_questions
      delete topics_questions[id]
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: topics_questions
        }
      })
    }
  }
  updateForm = (key, value) =>{
    let error = this.state.error
    delete error[key]
    if(key === 'chapter'){
      this.state.chapters.map(chapter =>{
        if(value === chapter.id){
          this.setState({
            topics: chapter.topics,
            testForm: {...this.state.testForm, [key]: value}
          })
        }
        return chapter
      })
    }else if(key === 'test_type' && value === 1){
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value, students: this.state.students.map(item => item.id)},
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value}
      })
    }
  }

  updateQuestion = (id, value, type) =>{
    this.setState({
      testForm: {...this.state.testForm,
        topics_questions: {...this.state.testForm.topics_questions, [id]: {...this.state.testForm.topics_questions[id],[type]:value}}
      }
    })
  }

  confirmStudents = () =>{
    this.setState({
      testForm: {...this.state.testForm, students: this.state.selectedStudents},
      showStudents: false
    })
  }

  selectStudent = (id) => {
    this.setState({
      selectedStudents: this.state.selectedStudents.includes(id) ? this.state.selectedStudents.filter(item => item !== id) : [...this.state.selectedStudents, id]
    })
  }

  selectAll = (flag) => {
    if(flag){
      this.setState({
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        selectedStudents: []
      })
    }
  }

  searchTests = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    this.getClassTestList({status: this.state.activeTab, test_name: evt.target.test_name.value})
  }

  clearTestForm = (flag) =>{
    // this.selectAll(true)
    this.setState({
      createTest: flag,
      testForm: {
        title: "",
        mode: 'add',
      	topics_questions: {
      	},
      	chapter: 0,
      	duration: 0,
      	min_correct_ans: 0,
      	scheduled_date_time: "",
        schedule: false,
        students: this.state.students.map(student => student.id),
        test_type: 1
      },
      current_page: 1
    })
  }
  handleChange = (event, newValue) => {
    if(newValue){
      this.getClassTestList({status: newValue, page: 1})
    }else{
      this.getClassTestList({page: 1})
    }
    this.setState({
        activeTab: newValue
    })
  }

  handlePageChange = (...args) =>{
    this.getClassTestList({page: args[1], status: this.state.activeTab})
    this.setState({
      current_page: args[1]
    })
  }

  onChangeRowsPerPage = (...args) =>{
    // this.getClassTestList({page: args[1], status: this.state.activeTab})
    // this.setState({
    //   current_page: args[1]
    // })
  }

  render(){
    const { activeTab, testForm, tests } = this.state
    var submitEnabled = false
    let maxQuestionsAllowed = 0
    Object.keys(testForm.topics_questions).map(key => {
      maxQuestionsAllowed = maxQuestionsAllowed + testForm.topics_questions[key].no_of_objective_questions + testForm.topics_questions[key].no_of_subjective_questions
      return key
    })
    if(testForm.title.trim() !== '' && testForm.students.length > 0 && testForm.chapter !== 0 && testForm.min_correct_ans !== 0 && testForm.min_correct_ans <= maxQuestionsAllowed && testForm.duration !== 0 && Object.keys(testForm.topics_questions).length){
      submitEnabled = true
      if(testForm.schedule){
        if(testForm.scheduled_date_time.toString().trim() !== ''){
          submitEnabled = true
        }else{
          submitEnabled = false
        }
      }
    }
    return(
      <div className="class_test_listing">
         <PageHeader
          title="Info"
          info="Info"
          action={tests.classTests.length && !this.state.loader ? <PrimaryButton style={{float: 'right'}} onClick={() => this.clearTestForm(true)} value="CREATE TEST NOW" type="button"/> : null}
          />
          {
          <div className="classTest-listing-wrapper">
              <Tabs
                    value={activeTab}
                    onChange={this.handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                    className="classTest-listing-tab"
                    >
                    <Tab label="Pre-learning Chapter" id="scrollable-auto-tab-0"/>
                    <Tab label="Post learning Topic"  id="scrollable-auto-tab-1"/>
                    <Tab label="Chapter Name" id="scrollable-auto-tab-2"/>
                    <Tab label="No. of Students" id="scrollable-auto-tab-2"/>
                </Tabs>
                <TabPanel value={activeTab} index={0} className="tab-panel">
                {/* <ClassTestListingRow /> */}
                  {/* {
                      <FilterAndSearchBar searchName="test_name" search={this.searchTests} placeholder="Search for test.."/>
                  }
                    {
                      !this.state.loader ?
                      !tests.classTests.length ?
                      <NoRecordFound
                        title="No Class Test Conducted Yet!"
                        content="You can conduct realtime or schedule test on completed chapters"
                        component={<PrimaryButton onClick={() => this.clearTestForm(true)} value="CREATE TEST NOW" type="button"/>}
                      />
                    : <ClassTestListingTable
                      headRow={classTestHeadRow}
                      tableData={
                          this.state.tests.classTests.map(test =>
                            <ClassTestListingRow deleteTest={() => this.deleteTest(test)} editTest={() => this.editTest(test)} row={test} />
                          )
                      }
                    />:null
                    } */}
                </TabPanel>
                <TabPanel value={activeTab} index={1} className="tab-panel">
                  {/* {
                      <FilterAndSearchBar searchName="test_name" search={this.searchTests} placeholder="Search for test.."/>
                  }
                  {
                    !this.state.loader ?
                    !tests.classTests.length ?
                    <NoRecordFound
                      title="No On-going Class Test Found"
                      content="You can conduct realtime or schedule test on completed chapters"
                      component={<PrimaryButton onClick={() => this.clearTestForm(true)} value="CREATE TEST NOW" type="button"/>}
                    />
                  : <ClassTestListingTable
                    headRow={classTestHeadRow}
                    tableData={
                        this.state.tests.classTests.map(test =>
                          <ClassTestListingRow row={test} />
                        )
                    }
                  />:null
                  } */}
                </TabPanel>
                <TabPanel value={activeTab} index={2} className="tab-panel">
                  {/* {
                      <FilterAndSearchBar searchName="test_name" search={this.searchTests} placeholder="Search for test.."/>
                  }
                  {
                    !this.state.loader ?
                    !tests.classTests.length ?
                    <NoRecordFound
                      title="No Scheduled Class Test Found"
                      content="You can conduct realtime or schedule test on completed chapters"
                      component={<PrimaryButton onClick={() => this.clearTestForm(true)} value="CREATE TEST NOW" type="button"/>}
                    />
                  : <ClassTestListingTable
                    headRow={classTestHeadRow}
                    tableData={
                        this.state.tests.classTests.map(test =>
                          <ClassTestListingRow deleteTest={() => this.deleteTest(test)} editTest={() => this.editTest(test)} row={test} />
                        )
                    }
                  />:null
                  } */}
                </TabPanel>
                <TabPanel value={activeTab} index={3} className="tab-panel">
                  {/* {

                      <FilterAndSearchBar searchName="test_name" search={this.searchTests} placeholder="Search for test.."/>

                  }
                  {
                    !this.state.loader ?
                    !tests.classTests.length ?
                    <NoRecordFound
                      title="No Completed Class Test Found"
                      content="You can conduct realtime or schedule test on completed chapters"
                      component={<PrimaryButton onClick={() => this.clearTestForm(true)} value="CREATE TEST NOW" type="button"/>}
                    />
                  : <ClassTestListingTable
                    headRow={classTestHeadRow}
                    tableData={
                        this.state.tests.classTests.map(test =>
                          <ClassTestListingRow row={test} />
                        )
                    }
                  />:null
                  } */}
                </TabPanel>
            </div>
        }
        {
          this.state.loader ?
            <Loader/>
          :null
        }
        <Dialog className="chapter-unlock-modal class-test-crud-modal select-students-modal" fullWidth={true} open={this.state.showStudents} onClose={() => this.setState({showStudents: false})}>
            <DialogTitle>
              Select Students
              <InputLabel htmlFor="username" onClick={() => this.selectAll(this.state.students.length === this.state.selectedStudents.length ? false : true)} className="student-list-select-all">
                <input
                    type="checkbox"
                    checked={this.state.students.length === this.state.selectedStudents.length}
                  />
                Select All Students
              </InputLabel>
            </DialogTitle>
              <DialogContent className="body">
                <div className="select-class-container">
                  {
                    this.state.students.map(student =>
                      <InputLabel htmlFor="username" className="student-list" onClick={() => this.selectStudent(student.id)}>
                        <input
                            type="checkbox"
                            checked={this.state.selectedStudents.includes(student.id)}
                          />
                        <span>{student.full_name}</span>
                        <span>ACME2019</span>
                      </InputLabel>
                    )
                  }
                </div>
              </DialogContent>
            <DialogActions className="footer">
              <Link onClick={() => this.setState({showStudents: false})} className="primary-link">Cancel</Link>
              <PrimaryButton disabled={!testForm.students.length} onClick={() => this.confirmStudents()} value="Add" type="button"/>
            </DialogActions>
        </Dialog>
        <Dialog className="chapter-unlock-modal class-test-crud-modal" fullWidth={true} open={this.state.deleteTest.flag} onClose={() => this.setState({showStudents: false})}>
          <DialogTitle></DialogTitle>
            <DialogContent>
              <DialogContentText>Delete Class Test <strong>{this.state.deleteTest.title}</strong></DialogContentText>
            </DialogContent>
            <DialogActions className="footer">
              <Link onClick={() => this.setState({deleteTest: {flag: false, title: ''}})} className="primary-link">Cancel</Link>
              <PrimaryButton onClick={() => this.deleteClassTest(this.state.deleteTest.id, this.state.activeTab)} value="Delete" type="button"/>
            </DialogActions>
        </Dialog>
      <Dialog className="chapter-unlock-modal class-test-crud-modal" fullWidth={true} maxWidth="md" open={this.state.createTest} onClose={() => this.setState({createTest: false})}>
          <DialogTitle>{testForm.mode === 'add' ? 'Create' : 'Edit'} class test<IconButton style={{float: 'right'}} onClick={() => this.setState({createTest: false})}><CloseIcon/></IconButton></DialogTitle>
            <DialogContent className="body">
            <DialogContentText>
              <div className="create-test-form">
                <div className="select-class-container" style={{width: '49%', display: 'inline-block'}}>
                  <InputLabel htmlFor="username">Title<span className="required-asterisk">*</span></InputLabel>
                  <input
                      id="title"
                      className="title"
                      defaultValue=""
                      placeholder="Enter Test Title"
                      value={testForm.title}
                      onChange={(evt) => this.updateForm('title', evt.target.value)}
                    />
                  <h6 className="error">{this.state.error.username}</h6>
                </div>
                <div style={{marginLeft: '2%', width: '49%', display: 'inline-block'}}>
                  <InputLabel htmlFor="username">Completed Chapter<span className="required-asterisk">*</span></InputLabel>
                  {
                    !this.state.chapters.length ?
                      <p>No Chapters Completed Yet!</p>
                    :<Select
                      value={this.state.testForm.chapter}
                      onChange={(evt) => this.updateForm('chapter', evt.target.value)}
                      input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                      className="chapter-select"
                    >
                    {
                      this.state.chapters.map((chapter, index) =>
                        <MenuItem key={index} value={chapter.id}>{chapter.title}</MenuItem>
                      )
                    }
                    </Select>
                  }
                </div>
              </div>
            </DialogContentText>
            {
              testForm.chapter !== '' || testForm.chapter !== 0 ?
              <div>
                <Grid container spacing={2} className="class-test-crud-topics">
                  <Grid item xs={10}><h5>Select the topics</h5></Grid>
                  <Grid item xs={1}><h5>Subjective Questions</h5></Grid>
                  <Grid item xs={1}><h5>Objective Questions</h5></Grid>
                  {
                    this.state.topics.map(item =>
                      <React.Fragment>
                        <Grid style={{cursor: 'pointer'}} item xs={10} onClick={(evt) => this.selectTopic(item.id)}>
                          <input type="checkbox" checked={this.state.testForm.topics_questions[item.id]!== undefined}/>
                          <span>{item.title}</span>
                        </Grid>
                        <Grid item xs={1}>
                          {
                            this.state.testForm.topics_questions[item.id] !== undefined ?
                              <input min={0} value={this.state.testForm.topics_questions[item.id].no_of_subjective_questions} onChange={(evt) => this.updateQuestion(item.id, parseInt(evt.target.value), 'no_of_subjective_questions')} type="number"/>
                            :null
                          }
                        </Grid>
                        <Grid item xs={1}>
                          {
                            this.state.testForm.topics_questions[item.id] !== undefined ?
                              <input min={0} value={this.state.testForm.topics_questions[item.id].no_of_objective_questions} onChange={(evt) => this.updateQuestion(item.id, parseInt(evt.target.value),'no_of_objective_questions')} type="number"/>
                            :null
                          }
                        </Grid>
                      </React.Fragment>
                    )
                  }
                </Grid>
                <Grid container spacing={2} className="class-test-crud-summary create-test-form">
                  <Grid item xs={6} style={{display: 'none'}}>
                    <InputLabel htmlFor="username">Class Test Type<span className="required-asterisk">*</span></InputLabel>
                    <div className="test-type-select">
                      {
                        this.state.classTestTypes.map(item =>
                          <span onClick={() => this.updateForm('test_type', item.value)}>
                            <input type="radio" style={{marginLeft: '0px'}} checked={testForm.test_type === item.value} /> {item.label}
                          </span>
                        )
                      }
                    </div>
                    <div>
                      <InputLabel htmlFor="username">
                        {this.state.testForm.students.length} Students Selected (<Link style={{cursor: 'pointer', color: '#4f9df0'}} onClick={() => this.setState({showStudents: true, selectedStudents: testForm.students})}>Change</Link>)
                      </InputLabel>
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                  </Grid>
                  <Grid item xs={6} style={{display: 'none'}}></Grid>
                  <Grid item xs={6}>
                    <div className="select-class-container">
                      <InputLabel htmlFor="username">Total Time ( Mins )<span className="required-asterisk">*</span></InputLabel>
                      <input
                          id="title"
                          className="title"
                          type="number"
                          min={0}
                          value={testForm.duration}
                          onChange={(evt) => this.updateForm('duration', parseInt(evt.target.value))}
                        />
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <div className="select-class-container">
                      <InputLabel htmlFor="username">Minimum Correct Answer<span className="required-asterisk">*</span></InputLabel>
                      <input
                          id="title"
                          className="title"
                          type="number"
                          min={0}
                          max={maxQuestionsAllowed}
                          value={testForm.min_correct_ans}
                          onChange={(evt) => this.updateForm('min_correct_ans', parseInt(evt.target.value))}
                        />
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                  </Grid>
                  <Grid item xs={6}>
                    <div >
                      <InputLabel htmlFor="username" style={{cursor: 'pointer'}} onClick={() => this.updateForm('schedule', !testForm.schedule)}>
                        <input
                            type="checkbox"
                            checked={testForm.schedule}
                          />
                        Schedule the test
                      </InputLabel>
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                  </Grid>
                  <Grid item xs={6}></Grid>
                  {
                    testForm.schedule ?
                    <Grid item xs={6}>
                      <div className="select-class-container">
                        <InputLabel htmlFor="username">Day & Time<span className="required-asterisk">*</span></InputLabel>
                          <DatePicker
                            selected={this.state.testForm.scheduled_date_time}
                            onChange={(date) => this.updateForm('scheduled_date_time', date)}
                            dateFormat="dd-MMM-yyyy, hh:mm a"
                            minDate={new Date()}
                            showTimeSelect={true}
                            className="up-datepicker"
                            popperPlacement="top-start"
                            minTime={new Date()}
                            maxTime={moment(this.state.testForm.scheduled_date_time).diff(moment().startOf('day'), 'days') > 0 ? new Date().setHours(24) : new Date(new Date().setHours(new Date().getHours() + (23 - new Date().getHours())))}
                          />
                        <h6 className="error">{this.state.error.username}</h6>
                      </div>
                    </Grid>
                    :null
                  }
                </Grid>
              </div>:null
            }
          </DialogContent>
          <DialogActions className="footer">
            <Link onClick={() => this.clearTestForm(false)} className="primary-link">Close</Link>
            <PrimaryButton disabled={!submitEnabled} onClick={() => {this.clearTestForm(false);this.createClassTest(testForm)}} value={testForm.mode === 'add' ? "SUBMIT" : "UPDATE"} type="button"/>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
