import React from 'react'
import { PageHeader } from '../../../shared_elements/ui_elements'
import { Grid } from '@material-ui/core';
import DashboardStatus from '../components/DashboardStatus'
import LearningProgressBar from '../components/LearningProgressBar'
import SyllabusProgressBar from '../components/SyllabusProgressBar'
import DashboardStudentCard from '../components/DashboardStudentCard'
import PendingChapterCard from '../components/PendingChapterCard'
import { withRouter } from 'react-router-dom'
import { NoRecordFound, Loader } from '../../../shared_elements/components'
import { storagePath } from '../../../constants'
import { getDashboardAttentionStudents, getDashboardTopPerformers, getDashboardLearning, getDashboardSyllabusStatus, getDashboardStudentStatus, getDashboardChapterStatus } from '../api_services'
class Dashboard extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      learningStatus: {},
      syllabusStatus: {},
      studentStatus: {},
      pendingChapters: [],
      neetAttention: [],
      topPerformers:{
        class_toppers: [],
        section_toppers: []
      },
      loader: false
    }
    this.getDashboardLearning = getDashboardLearning.bind(this)
    this.getDashboardSyllabusStatus = getDashboardSyllabusStatus.bind(this)
    this.getDashboardStudentStatus = getDashboardStudentStatus.bind(this)
    this.getDashboardChapterStatus = getDashboardChapterStatus.bind(this)
    this.getDashboardAttentionStudents = getDashboardAttentionStudents.bind(this)
    this.getDashboardTopPerformers = getDashboardTopPerformers.bind(this)
  }

  componentDidMount(){
    // this.getDashboardLearning()
    // this.getDashboardSyllabusStatus()
    // this.getDashboardStudentStatus()
    // this.getDashboardChapterStatus()
    // this.getDashboardAttentionStudents()
    // this.getDashboardTopPerformers()
  }

  render(){
    const { learningStatus, syllabusStatus, studentStatus, pendingChapters, neetAttention, topPerformers } = this.state
    const classInfo = JSON.parse(localStorage.getItem('syllabus'))
    return(
      <div className="dashboard" style={{padding: '0'}}>
        {
          this.state.loader ?
            <Loader/>
          :null
        }
        <PageHeader title="Dashboard"/>
        <br/>
        <Grid container justify="center"  spacing={3}>
        <Grid item xs={3} >
            <div className="learning-status" >
              <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}>
            <DashboardStatus data={[{label:"Total Classrooms"}]}/>
            </Grid>
                          </div>
                  </Grid>
          <Grid item xs={3}>
              <div className="learning-status"> 
                  <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}>
                        <DashboardStatus data={[{label:"Total Teacher"},
                         {label:"Active"},
                          {label:"In-Active"}]}/>
                  </Grid>
              </div>
          </Grid>
          <Grid item xs={3}>
            <div className="learning-status">
              <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}>
            <DashboardStatus data={[{label:"Total Students"},
                         {label:"Active"},
                          {label:"In-Active"}]}/>
                          </Grid>
                          </div>
                  </Grid>
          <Grid item xs={3}>
              <div className="syllabus-status">
                Teaching Learning Effectiveness
                <SyllabusProgressBar status={{}} />
              </div>
          </Grid>
        </Grid>
        <Grid container spacing={3} style={{marginTop: '15PX'}}>
          <Grid item xs={3} >
            <div className="dashboard-listing-wrapper" style={{height: window.innerHeight - 300,overflow: 'auto'}}> 
              Top Performing Classrooms in Biology : 
              </div>
          </Grid>
          <Grid item xs={3}>
            <div className="dashboard-listing-wrapper" style={{height: window.innerHeight - 300,overflow: 'auto'}}>
            Top Performing Classrooms in Chemistry :
            </div>
          </Grid>
          <Grid item xs={2}>
            <div className="dashboard-listing-wrapper" style={{height: window.innerHeight - 300,overflow: 'auto'}}>
            Top Performing Classrooms in Maths :
              </div>
          </Grid>
          <Grid item xs={2}>
            <div className="dashboard-listing-wrapper" style={{height: window.innerHeight - 300,overflow: 'auto'}}>
            Top 3 Performing teachers of the Grade
              </div>
          </Grid>
          <Grid item xs={2}>
            <div className="dashboard-listing-wrapper" style={{height: window.innerHeight - 300,overflow: 'auto'}}>
            Top 3 Performing teachers of the school
              </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withRouter(Dashboard)
