import React, { Component } from 'react'
import { IconButton, Link,CardActions,Button, Grid, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, InputLabel, Select, OutlinedInput, MenuItem,  Tabs, Tab } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import { PageHeader, PrimaryButton, TabPanel } from '../../../shared_elements/ui_elements'
import { NoRecordFound, Loader } from '../../../shared_elements/components'
import DatePicker from "react-datepicker";
import { getTestChapters, getClassTestList, createClassTest, getConstants, deleteClassTest } from '../api_services'
// import "react-datepicker/dist/react-datepicker.css";
import FilterAndSearchBar from '../components/FilterAndSearchBar'
import ClassTestListingTable from '../components/ClassTestListingTable'
import ClassTestListingRow from '../components/ClassTestListingRow'
import DashboardStatus from '../components/DashboardStatus'
import LearningProgressBar from '../components/LearningProgressBar'
import SyllabusProgressBar from '../components/SyllabusProgressBar'
import moment from 'moment'
import ErrorIcon from '@material-ui/icons/Error';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import ErrorTwoToneIcon from '@material-ui/icons/ErrorTwoTone';
const classTestHeadRow=["Test", "Status", "Date", "Attended", "Passed", "Failed"]
export default class ClassTest extends Component{
  constructor(props){
    super(props)
    this.state = {
      complete:0,
      createTest: false,
      testForm: {
        title: "",
        mode:'add',
      	topics_questions: {
      	},
      	chapter: 0,
      	duration: 10,
      	min_correct_ans: 1,
      	scheduled_date_time: "",
        schedule: false,
        students: [],
        test_type: 1
      },
      showStudents: false,
      activeTab: 0,
      chapters: [],
      topics: [],
      tests:{
        classTests: [],
        pagination: {
          total: 0,
          current_page: 1
        }
      },
      error:{},
      loader: false,
      students: [],
      selectedStudents: [],
      classTestTypes: [],
      deleteTest: {
        title: '',
        flag: false
      }
    }
    this.getTestChapters = getTestChapters.bind(this)
    this.getClassTestList = getClassTestList.bind(this)
    this.createClassTest = createClassTest.bind(this)
    this.deleteClassTest = deleteClassTest.bind(this)
    this.getConstants = getConstants.bind(this)
  }

  componentDidMount(){
    // this.getTestChapters()
    // this.getClassTestList()
    // this.getConstants('class_test_type')
  }

  editTest = (test) =>{
    this.setState({testForm: {...test, mode: 'edit', schedule: true, scheduled_date_time: new Date(test.scheduled_date_time)}, createTest: true})
    this.state.chapters.map(chapter =>{
      if(test.chapter === chapter.id){
        this.setState({
          topics: chapter.topics,
        })
      }
      return chapter
    })
  }

  deleteTest = (test) =>{
    this.setState({deleteTest: {title: test.title, flag: true, id: test.id}})
  }

  selectTopic = (id) =>{
    if(!this.state.testForm.topics_questions[id]){
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: {...this.state.testForm.topics_questions, [id]: {no_of_objective_questions: 0, no_of_subjective_questions: 0}}
        }
      })
    }else{
      let topics_questions = this.state.testForm.topics_questions
      delete topics_questions[id]
      this.setState({
        testForm: {...this.state.testForm,
          topics_questions: topics_questions
        }
      })
    }
  }
  updateForm = (key, value) =>{
    let error = this.state.error
    delete error[key]
    if(key === 'chapter'){
      this.state.chapters.map(chapter =>{
        if(value === chapter.id){
          this.setState({
            topics: chapter.topics,
            testForm: {...this.state.testForm, [key]: value}
          })
        }
        return chapter
      })
    }else if(key === 'test_type' && value === 1){
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value, students: this.state.students.map(item => item.id)},
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        error: error,
        testForm: {...this.state.testForm, [key]: value}
      })
    }
  }

  updateQuestion = (id, value, type) =>{
    this.setState({
      testForm: {...this.state.testForm,
        topics_questions: {...this.state.testForm.topics_questions, [id]: {...this.state.testForm.topics_questions[id],[type]:value}}
      }
    })
  }

  confirmStudents = () =>{
    this.setState({
      testForm: {...this.state.testForm, students: this.state.selectedStudents},
      showStudents: false
    })
  }

  selectStudent = (id) => {
    this.setState({
      selectedStudents: this.state.selectedStudents.includes(id) ? this.state.selectedStudents.filter(item => item !== id) : [...this.state.selectedStudents, id]
    })
  }

  selectAll = (flag) => {
    if(flag){
      this.setState({
        selectedStudents: this.state.students.map(item => item.id)
      })
    }else{
      this.setState({
        selectedStudents: []
      })
    }
  }

  searchTests = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    this.getClassTestList({status: this.state.activeTab, test_name: evt.target.test_name.value})
  }

  clearTestForm = (flag) =>{
    // this.selectAll(true)
    this.setState({
      createTest: flag,
      testForm: {
        title: "",
        mode: 'add',
      	topics_questions: {
      	},
      	chapter: 0,
      	duration: 0,
      	min_correct_ans: 0,
      	scheduled_date_time: "",
        schedule: false,
        students: this.state.students.map(student => student.id),
        test_type: 1
      },
      current_page: 1
    })
  }
  handleChange = (event, newValue) => {
    if(newValue){
      this.getClassTestList({status: newValue, page: 1})
    }else{
      this.getClassTestList({page: 1})
    }
    this.setState({
        activeTab: newValue
    })
  }

  handlePageChange = (...args) =>{
    this.getClassTestList({page: args[1], status: this.state.activeTab})
    this.setState({
      current_page: args[1]
    })
  }

  onChangeRowsPerPage = (...args) =>{
    // this.getClassTestList({page: args[1], status: this.state.activeTab})
    // this.setState({
    //   current_page: args[1]
    // })
  }
  
  viewButton = ()=>{
   this.props.props.history.push('studentprofile')
    console.log(this.props)
 
  }
  
  render(){
    const { activeTab, testForm, tests } = this.state
    var submitEnabled = false
    let maxQuestionsAllowed = 0
    Object.keys(testForm.topics_questions).map(key => {
      maxQuestionsAllowed = maxQuestionsAllowed + testForm.topics_questions[key].no_of_objective_questions + testForm.topics_questions[key].no_of_subjective_questions
      return key
    })
    if(testForm.title.trim() !== '' && testForm.students.length > 0 && testForm.chapter !== 0 && testForm.min_correct_ans !== 0 && testForm.min_correct_ans <= maxQuestionsAllowed && testForm.duration !== 0 && Object.keys(testForm.topics_questions).length){
      submitEnabled = true
      if(testForm.schedule){
        if(testForm.scheduled_date_time.toString().trim() !== ''){
          submitEnabled = true
        }else{
          submitEnabled = false
        }
      }
    }
    return(
      <div className="dashboard" style={{padding: '0'}}>
      {
        this.state.loader ?
          <Loader/>
        :null
      }
      <PageHeader title="ClassRoom"/>
      <p>Info</p>
      <br/>
      <Grid container justify="center" className="Grid-container-classroom" spacing={3}>
      <Grid item xs={4} >
          <div className="learning-status"  style={{display:'block',backgroundColor:'white'}} >
            {/* <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}> */}
          {/* <DashboardStatus data={[{label:"Total Classrooms"}]}/> */}
          <h3> <p>Biology</p></h3>
         
          
          <p>Inprogress:-</p>
          <p>Pre-learning:-
          <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
          </p>
          {/* <div className="syllabus-status">
                Teaching Learning Effectiveness
                
              </div> */}
          <p>Post-learning:-
          <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="secondary"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
          </p>
          <div style={{display:'flex',width:'100%'}}>
          <Grid item xs={4}>
            <p>Totaltopic:15</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Completed:10</span></p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Ongoing:5</span></p>
          </Grid>
          </div>
          <CardActions style={{display:'block',textAlign:'center'}}>
          <Button variant="contained" size="small" className="btn" onClick={this.viewButton} className="btn-primary">View</Button>
         
        
          </CardActions>
        
          {/* </Grid> */}
                        </div>
                </Grid>
        <Grid item xs={4}>
        <div className="learning-status" style={{display:'block',backgroundColor:'white'}}>
            {/* <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}> */}
          {/* <DashboardStatus data={[{label:"Total Classrooms"}]}/> */}
          <h3> <p>Biology</p></h3>
          
          <p>Inprogress:-</p>
          <p>Pre-learning:-
          <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
          </p>
          {/* <div className="syllabus-status">
                Teaching Learning Effectiveness
                
              </div> */}
               <p>Post-learning:-
                <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
            </p>
          {/* <p>Post-learning:-
          <SyllabusProgressBar status={{}} />
          </p>
          <ErrorIcon color="secondary"/> */}
              <div style={{display:'flex',width:'100%'}}>
          <Grid item xs={4}>
            <p>Totaltopic:15</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Completed:10</span></p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Ongoing:5</span></p>
          </Grid>
          </div>
          <CardActions style={{display:'block',textAlign:'center'}}>
          <Button variant="contained" size="small" className="btn" onClick={this.viewButton}>View</Button>
        
          </CardActions>
            </div>
        </Grid>
        <Grid item xs={4}>
        <div className="learning-status" style={{display:'block',backgroundColor:'white'}}>
            {/* <Grid item xs={4} style={{paddingRight: '20px', paddingLeft: '20px'}}> */}
          {/* <DashboardStatus data={[{label:"Total Classrooms"}]}/> */}
          <h3> <p>Biology</p></h3>
          
          <p>Inprogress:-</p>
          <p style={{display:'inline'}}>Pre-learning:-
          <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
         
          </p>
          
          
          
          {/* <div className="syllabus-status">
                Teaching Learning Effectiveness
                
              </div> */}
          <p>Post-learning:-
          <div className="learning-progress-bar">
                <div className="progress performers" style={{background: this.state.complete <=100 ? `linear-gradient(90deg, #96B375 ${this.state.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '85%'}}>

                </div>
                <span style={{float:'right',marginTop:'6px'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${this.state.complete <=100 ? parseFloat(this.state.complete).toFixed(2) : 0}% complete`}</p>
          </p>
          <div style={{display:'flex',width:'100%'}}>
          <Grid item xs={4}>
            <p>Totaltopic:15</p>
            
          </Grid>
          
          <Grid item xs={4} >
            <p><span >Completed:10</span></p>
          </Grid>
          <Grid item xs={4}>
            <p style={{marginLeft:'5px'}}><span>Ongoing:5</span></p>
          </Grid>
          </div>
          <CardActions style={{display:'block',textAlign:'center'}}>
          <Button variant="contained" size="small" className="btn" onClick={this.viewButton} >View</Button>
          
        
          </CardActions>
                        </div>
                </Grid>
  
      </Grid>
     
    </div>
      
    )
  }
}
