import Students from './containers/Students'
import StudentProfile from './containers/StudentProfile'
import Dashboard from './containers/Dashboard'
import Syllabus from './containers/Syllabus'
import { AppLayout, ProtectedRoute } from '../../hocs'
import Chapter from './containers/Chapter'
import ClassTestDetail from './containers/ClassTestDetail'
import Announcement from './containers/Announcement'
import NeedAttention from './containers/NeedAttention'
import ClassTest from './containers/ClassTest'
import Prelearnview from './containers/Prelearnview'
import Worksheet from './containers/Worksheet'

export default [
  {
    path: '/students',
    component: ProtectedRoute(AppLayout(Students)),
    key: 'students'
  },
  {
    path: '/students/:studentSlug',
    component: ProtectedRoute(AppLayout(StudentProfile)),
    key: 'student_detail'
  },
  {
    path: '/studentprofile',
    component: ProtectedRoute(AppLayout(StudentProfile)),
    key: 'student_detail'
  },
  {
    path: '/dashboard',
    component: ProtectedRoute(AppLayout(Dashboard)),
    key: 'dashboard'
  },
  {
    path: '/syllabus',
    component: ProtectedRoute(AppLayout(Syllabus)),
    key: 'syllabus'
  },
  {
    path: '/syllabus/chapter/:chapterSlug',
    component: ProtectedRoute(AppLayout(Chapter)),
    key: 'chapter_detail'
  },
  {
    path: '/class_test/:classTestSlug',
    component: ProtectedRoute(AppLayout(ClassTestDetail)),
    key: 'classTest-status'
  },
  {
    path: '/announcement',
    component: ProtectedRoute(AppLayout(Announcement)),
    key: 'announcement'
  },
  {
    path: '/attention',
    component: ProtectedRoute(AppLayout(NeedAttention)),
    key: 'need_attention'
  },
  {
    path: '/class_test',
    component: ProtectedRoute(AppLayout(ClassTest)),
    key: 'class_test'
  },
  {
    path: '/learn_view',
    component: ProtectedRoute(AppLayout(Prelearnview)),
    key: 'learnview_test'
  },
  {
    path: '/worksheets',
    component: ProtectedRoute(AppLayout(Worksheet)),
    key: 'worksheets'
  }
]
