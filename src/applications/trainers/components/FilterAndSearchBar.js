import React from 'react'
import { Grid, Button, InputBase } from '@material-ui/core'
export default function FilterAndSearchBar(props){
    return(
      <form onSubmit={props.search} style={props.style}>
        <Grid container className="filter-search-bar" style={{margin: '10px', width: '98%'}}>
                <Grid item xs={10}>
                    <InputBase
                        placeholder={props.placeholder}
                        className="search-bar"
                        name={props.searchName}
                        inputProps={{ 'aria-label': 'search' }}
                    />

                </Grid>
                <Grid item xs={2}>
                    <Button variant="contained" type="submit" size="small" className="filter-btn">Search</Button>
                </Grid>
        </Grid>
        </form>
    )
}
