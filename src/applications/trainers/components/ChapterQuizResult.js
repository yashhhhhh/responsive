import React from 'react'
import { Grid } from '@material-ui/core'
import { ExpansionList } from '../../../shared_elements/ui_elements'
import QuizAnswerOption from './QuizAnswerOption'
import QuizSubjectiveAnswer from './QuizSubjectiveAnswer'
import { optionNumbers } from '../../../constants'
import MathJax from 'react-mathjax-preview-ds'
export default function ChapterQuizResult(props){
    return(
      <Grid item xs={12} style={{marginBottom: '10px'}}>
        <ExpansionList toggle={Object.keys(props.quizData.student_answer).length} title={
            <Grid container style={{width:'97%'}}>
                <Grid item xs={1}><h3>#{props.index !== null && props.index !== undefined ? props.index + 1 : null}</h3></Grid>
                <Grid item xs={2}><h3>{props.quizData.option_type === 1 ? 'Multi-Choice' : 'Descriptive'}</h3></Grid>
              <Grid item xs={7}><MathJax math={props.quizData.question}></MathJax></Grid>
              <Grid item xs={2} style={{textAlign:'right'}}>{Object.keys(props.quizData.student_answer).length ? props.quizData.student_answer.status === 1 ? <span style={{background: '#7ED321', color: '#fff'}}>Correct Answer</span> : <span style={{background: '#FD7082', color: '#fff'}}>Incorrect Answer</span> : <span style={{background: '#ACACAC', color: '#fff'}}>Not Attempted</span>}</Grid>
            </Grid>
        } className="chapter-quiz-stuent-result">
            <Grid container className="quiz-result-detail">
                <Grid item xs={3} style={{maxWidth: '23%'}}></Grid>
                <Grid item xs={9} style={{display: 'flex'}}>
                    {props.quizData.option_type === 1 ? props.quizData.options.map((option, index)=>{
                        return <QuizAnswerOption attempted={props.quizData.student_answer.options && props.quizData.student_answer.options.includes(option.id.toString())} className={props.quizData.student_answer.options && props.quizData.student_answer.options.includes(option.id.toString()) ? option.is_answer ?  "quiz-answer-option success-answer" : "quiz-answer-option failed-answer" : option.is_answer ? 'quiz-answer-option success-answer' : "quiz-answer-option"} col={3} title={`Option ${optionNumbers[index]}`} value={option.value} />
                    }) : <React.Fragment>
                      <QuizSubjectiveAnswer className={Object.keys(props.quizData.student_answer).length && props.quizData.student_answer.status === 1 ?  "quiz-answer-option success-answer" : "quiz-answer-option failed-answer"} col={6} title={`Answer`} value={props.quizData.student_answer.answer} />
                      <QuizSubjectiveAnswer type="correct" className={"quiz-answer-option success-answer"} col={6} title={`Correct Answer`} value={props.quizData.answer} />
                    </React.Fragment>
                    }
                </Grid>

            </Grid>
        </ExpansionList>
    </Grid>)
}
