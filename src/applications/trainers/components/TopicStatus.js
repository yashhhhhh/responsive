import StudentStatusCard from './StudentStatusCard'
import React from 'react'
import { withRouter } from 'react-router-dom'
import { Grid } from '@material-ui/core';
function TopicStatus(props) {
  return (
    <Grid container spacing={2} className="topic-student-status-wrapper">
      <Grid item style={{ width: props.activeTab == 1 ? '20%' : '33.33%' }} className="student-card-wrapper"><p>Learning ({props.topicStudents.learning ? props.topicStudents.learning.length : 0})</p>
        {
          props.topicStudents.learning && props.topicStudents.learning.length ?
            props.topicStudents.learning.map((student, index) =>
              <StudentStatusCard
                status={student.digital_card_status == 1 ? <p>Completed</p> : <p>Learning</p>}
                student={student}
                key={'learning_' + index}
                type={props.activeTab == 1 ? "post" : ''}
                redirectTo={() => props.history.push(`/students/${student.id}`)}
              />
            )
            : null
        }
      </Grid>
      <Grid item style={{ width: props.activeTab == 1 ? '20%' : '33.33%' }} className="student-card-wrapper"><p>Quiz Level 1 ({props.topicStudents.quiz ? props.topicStudents.quiz[1].length : 0})</p>
        {
          props.topicStudents.quiz && props.topicStudents.quiz[1].length ?
            props.topicStudents.quiz[1].map((student, index) =>
              <StudentStatusCard
                status={student.status == 1 ? <p>Completed</p> : (student.status == 2 || student.status == 3) ? <p style={{ color: 'red' }}>{!student.is_quiz_in_progress ? 'Failed' : 'In Progress'}</p> : <p>Pending</p>}
                student={student}
                key={'q1_' + index}
                quizFlag={[1, 2].includes(student.status)}
                redirectTo={() => props.history.push(`/students/${student.id}`)}
                type={props.activeTab == 1 ? "post" : ''}
                getStudentQuiz={() => props.getStudentQuiz(student.id, 1)}
              />
            )
            : null
        }
      </Grid>
      <Grid item style={{ width: props.activeTab == 1 ? '20%' : '33.33%' }} className="student-card-wrapper"><p>Quiz Level 2 ({props.topicStudents.quiz ? props.topicStudents.quiz[2].length : 0})</p>
        {
          props.topicStudents.quiz && props.topicStudents.quiz[2].length ?
            props.topicStudents.quiz[2].map((student, index) =>
              <StudentStatusCard
                status={student.status == 1 ? <p>Completed</p> : (student.status == 2 || student.status == 3) ? <p style={{ color: 'red' }}>{!student.is_quiz_in_progress ? 'Failed' : 'In Progress'}</p> : <p>Pending</p>}
                student={student}
                key={'q2_' + index}
                quizFlag={[1, 2].includes(student.status)}
                type={props.activeTab == 1 ? "post" : ''}
                redirectTo={() => props.history.push(`/students/${student.id}`)}
                getStudentQuiz={() => props.getStudentQuiz(student.id, 2)}
              />
            )
            : null
        }
      </Grid>
      {
        props.activeTab == 1 ?
          <Grid item style={{ width: '20%' }} className="student-card-wrapper"><p>Quiz Level 3 ({props.topicStudents.quiz ? props.topicStudents.quiz[3].length : 0})</p>
            {
              props.topicStudents.quiz && props.topicStudents.quiz[3].length ?
                props.topicStudents.quiz[3].map((student, index) =>
                  <StudentStatusCard
                    status={student.status == 1 ? <p>Completed</p> : (student.status == 2 || student.status == 3) ? <p style={!student.is_quiz_in_progress ? { color: 'red' } : {}}>{!student.is_quiz_in_progress ? 'Failed' : 'In Progress'}</p> : <p>Pending</p>}
                    student={student}
                    key={'q2_' + index}
                    quizFlag={[1, 2].includes(student.status)}
                    type={props.activeTab == 1 ? "post" : ''}
                    redirectTo={() => props.history.push(`/students/${student.id}`)}
                    getStudentQuiz={() => props.getStudentQuiz(student.id, 3)}
                  />
                )
                : null
            }
          </Grid>
          : null
      }
      {
        props.activeTab == 1 ?
          <Grid item style={{ width: '20%' }} className="student-card-wrapper"><p>Need Attention ({props.topicStudents.need_attention ? props.topicStudents.need_attention.length : 0})</p>
            {
              props.topicStudents.need_attention && props.topicStudents.need_attention.length ?
                props.topicStudents.need_attention.map((student, index) =>
                  <StudentStatusCard
                    status="Need Help"
                    student={student}
                    type={props.activeTab == 1 ? "post" : ''}
                    key={'learning_' + index}
                    redirectTo={() => props.history.push(`/attention`)}
                    helpStudent={() => props.helpStudent(student)}
                  />
                )
                : null
            }
          </Grid>
          : null
      }
    </Grid>
  )
}

export default withRouter(TopicStatus)
