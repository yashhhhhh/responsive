import React from 'react'
import { Grid } from '@material-ui/core';
import ChapterQuizResult from './ChapterQuizResult'
export default function StatusTable(props){
    return(
      <p className="class-test-status-table">
        <Grid container className="class-test-status-table-header">
                    <Grid item xs={1} size="small">Sl No.</Grid>
                    <Grid item xs={2} >Type</Grid>
                    <Grid item xs={7}>Question</Grid>
                    <Grid item xs={2} style={{paddingLeft: '15px'}}>Result</Grid>
        </Grid>
        <Grid container>
            {props.quizData.quiz && props.quizData.quiz.map((row,index) => {
              return <ChapterQuizResult quizData={row} key={index} index={index}/>
            })}
        </Grid>
      </p>
    )
}
