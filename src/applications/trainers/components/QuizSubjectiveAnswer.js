import React from 'react'
import { Grid } from '@material-ui/core'
import MathJax from 'react-mathjax-preview-ds'
export default function QuizSubjectiveAnswer(props){
    return(
        <Grid item xs={props.col} className={props.className}>
            <span>{props.title}</span>
            {
              props.type == "correct" ?
              <MathJax math={props.value}></MathJax>
              :props.value ? 
                <MathJax math={props.value.includes('|||') ? props.value.replace('|||', ',') : props.value}></MathJax>
              :<p>--</p>
            }
        </Grid>
    )
}
