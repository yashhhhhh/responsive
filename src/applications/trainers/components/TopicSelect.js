import React, { Fragment } from 'react'
import { Grid, FormControl, Select, OutlinedInput, MenuItem, Link } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons'
export default function TopicSelect(props) {
    console.log(props);
    return (
        <Fragment>
            <Link className="primary-link" onClick={() => { props.handleTopicOpen('') }} style={{ marginBottom: '8px' }}><ArrowBack style={{ verticalAlign: 'sub' }} />{`Back to ${props.activeTab == 0 ? 'Pre' : 'Post'}-learning Summary Of ${props.chapter.title}`}</Link>
            <Grid container>
                <Grid item xs={3}>
                    <FormControl className="select-item">
                        <Select
                            value={props.value}
                            onChange={props.handleTopicChange}
                            input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                        >
                            {props.chapter.topics.map((topic, index) => {
                                return <MenuItem key={index} value={topic.id}>{topic.title}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={8} style={{ alignSelf: 'center', display: 'none' }}>
                    <ul>
                        <li onClick={() => props.getDigicard(props.value)}>View DigiCards</li>
                        <li >View Level 1 Quiz</li>
                        <li >View Level 2 Quiz</li>
                    </ul>
                </Grid>
            </Grid>
        </Fragment>

    )
}
