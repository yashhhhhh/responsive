import React from 'react'
import { Grid, Button } from '@material-ui/core';
export default function NeedAttentionListItem(props){
    const {data} = props
    return(
        <Grid item xs={12} className="need-attention-item-wrapper">
            <p>{data.chapter.title} ({data.learning_type == 1 ? 'Pre' : 'Post'} Learning)</p>
            <Grid container>
                <Grid item xs={6}><h4>{data.user.first_name} {data.user.last_name}</h4></Grid>
                <Grid item xs={4}><span>{data.status ? data.status : null}</span></Grid>
                <Grid item xs={2}>
                  {
                    props.helpStudent ?
                    <Button variant="contained" onClick={props.helpStudent} size="small" className="action-button">{props.type == 'edit' ? "Action" : 'View' }</Button>
                    :null
                  }
                </Grid>
            </Grid>
        </Grid>
    )
}
