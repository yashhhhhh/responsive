import React from 'react'
import moment from 'moment'

export default function TimeLineList(props){
  return(
    <div className="timeline-content">
      <div className="content">
        {props.action.action}
        <p>{props.action.time ? moment(props.action.time).format('DD MMM YYYY, HH:mm a') : '--'}</p>
      </div>
    </div>
  )
}
