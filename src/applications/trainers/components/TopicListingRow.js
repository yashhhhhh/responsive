import React from 'react'
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { withRouter } from 'react-router-dom'
function TopicListingRow(props){
    return(
        <TableRow>
                <TableCell>
                    <span  onClick={(e)=>{props.handleChapterOpen(1)}}>{props.topicName}</span>
                    <ul>
                        <li >View DigiCards</li>
                        <li >View Level 1 Quiz</li>
                        <li >View Level 2 Quiz</li>
                    </ul>
                </TableCell>
                <TableCell align="center">11</TableCell>
                <TableCell align="center">6</TableCell>
                <TableCell align="center">5</TableCell>
                <TableCell align="center">1</TableCell>
        </TableRow>
    )
}
export default withRouter(TopicListingRow)
