import React from 'react'
import { Grid } from '@material-ui/core'
import moment from 'moment'

export default function ClassTestList(props){
  return(
    <Grid container spacing={1} className="student-test">
      <Grid item xs={6}>
        <span>Chapter {props.test.chapter && props.test.chapter.title ? props.test.chapter.title : ''}</span>
        <h4>{props.test.title}</h4>
      </Grid>
      <Grid item xs={4}>
        <span>Conducted On</span>
        <h4>{props.test.scheduled_date_time ? moment(props.test.scheduled_date_time).format('YYYY MMM DD, hh:mm a') : '--'}</h4>
      </Grid>
      <Grid item xs={2}>
        <span>Result</span>
        <h4>{props.test.status === 1 ? <span className="success-badge">Passed</span> : props.test.status === 2 ? <span className="error-badge">Failed</span> : moment(props.test.scheduled_date_time).diff(moment(), 'minutes') > 0 ? <span className="error-badge" style={{background: 'rgb(246, 223, 96)'}}>Scheduled</span> : <span className="warning-badge">Not Attended</span>}</h4>
      </Grid>
    </Grid>
  )
}
