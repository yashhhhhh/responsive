import React from 'react'
import { TableRow, TableCell } from '@material-ui/core';
import { Link } from 'react-router-dom'
import moment from 'moment'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
export default function ClassTestListingRow({row, editTest, deleteTest}){
  const statuses = [
    "",
    'On Going',
    "Scheduled",
    "Completed",
  ]
    return(
        <TableRow>
            <TableCell><p>Chapter {"hello"}</p><h3>{"hello"}</h3></TableCell>
            <TableCell></TableCell>
            {/* <TableCell>{moment(row.scheduled_date_time).format('DD MMM YYYY, hh:mm a')}</TableCell> */}
            <TableCell>{"hello"}</TableCell>
            <TableCell>{"hello"}</TableCell>
            <TableCell>{"hello"}</TableCell>
            {/* <TableCell align='right' style={{paddingRight:'0px'}}> */}
              {/* <Link to={`/class_test/${row.id}`}>VIEW</Link> */}
              {/* {
                row.status === 2 ?
                  <React.Fragment>
                    <Link onClick={editTest} className="edit-test"><EditIcon/></Link>
                    <Link onClick={deleteTest} className="delete-test"><DeleteIcon/></Link>
                  </React.Fragment>
                :null
              } */}
            {/* </TableCell> */}
        </TableRow>
    )
}
