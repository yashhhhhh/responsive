import React from 'react'
import { Grid } from '@material-ui/core'
export default function PendingChapterCard({chapter}){
    return(
        <li className="pending-chapter-wrapper">
            <Grid container spacing={0} >
                <Grid item xs={1}>
                    <div className="circle-icon"></div>
                </Grid>
                <Grid item xs={11}>
                    <p><h3>Chapter {chapter.index}:</h3> {chapter.title}</p>
                </Grid>
            </Grid>
        </li>
    )
}
