import React from 'react'
import { Grid, Link } from '@material-ui/core'
import { storagePath } from '../../../constants'
import MathJax from 'react-mathjax-preview-ds'
export default function DigiCard(props){
  return(
    <Grid item xs={4}>
    <div className="digicard-item">
    <div style={{textAlign: 'center'}}><img src={props.card.featured_image ? props.card.featured_image : storagePath + 'no_image_placholder.png'} alt=''/></div>
    <div>
      <h4>{props.card.title}</h4>
      <div style={{padding: '0px', height: '150px', overflow: 'auto'}}><MathJax math={props.card.excerpt} /></div>
      <Link onClick={props.readMore}>Read more</Link>
    </div>
    </div>
    </Grid>
  )
}
