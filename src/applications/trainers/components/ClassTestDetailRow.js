import React from 'react'
import { TableRow, TableCell } from '@material-ui/core';
export default function ClassTestDetailRow(props){
    const { row } = props
    return(
        <TableRow>
            <TableCell>{row.name}</TableCell>
            <TableCell>{row.attempted}</TableCell>
            <TableCell>{row.right}</TableCell>
            <TableCell>{row.wrong}</TableCell>
            <TableCell>{row.result}</TableCell>
            <TableCell align='right' style={{paddingRight:'0px'}}>
              {
                row.status !== 0 ? <a href="javscript:void(0)" onClick={()=> props.handleStudentOpen(row.id)}>VIEW</a> : null
              }
            </TableCell>
        </TableRow>
    )
}
