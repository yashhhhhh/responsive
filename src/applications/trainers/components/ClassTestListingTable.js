import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export default function ClassTestListingTable(props){
    return(
        <Table className="class-test-listing-table">
            <TableHead>
                <TableRow>
                    {props.headRow.map((item, index) => {
                        return <TableCell key={index}>{item}</TableCell>
                    })}
                    <TableCell></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {props.tableData}
            </TableBody>
        </Table>
    )
}
