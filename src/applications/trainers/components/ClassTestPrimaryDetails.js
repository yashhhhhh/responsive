import React from 'react'
import { Grid } from '@material-ui/core'
import ClassTestTopic from './ClassTestTopic'
import moment from 'moment'
export default function ClassTestPrimaryDetails({detail, topics}){
    return(
    <div className="classTest-primary-detail" >
        <Grid container spacing={4} style={{marginBottom: '25px', marginTop: '0px'}}>
                <Grid item className="classTest-detail-item">
                    <p>Chapter</p>
                    <h3>Chapter {detail.chapter.index} : {detail.chapter.title}</h3>
                </Grid>
                <Grid item className="classTest-detail-item">
                     <p>No. of Questions</p>
                    <h3>{detail.no_of_subjective_questions + detail.no_of_objective_questions}</h3>
                </Grid>
                <Grid item className="classTest-detail-item">
                    <p>Passing Marks</p>
                    <h3>{detail.pass_mark}</h3>
                </Grid>
                <Grid item className="classTest-detail-item">
                    <p>Total Time (Mins)</p>
                    <h3>{detail.total_time}</h3>
                </Grid>
                <Grid item className="classTest-detail-item">
                    <p>Date & Time</p>
                  <h3>{detail.datetime ? moment(detail.datetime).format('MMM DD YYYY, hh:mm a') : '--'}</h3>
                </Grid>
        </Grid>
        <Grid container className="classTest-topics-wrapper">
            <span style={{color: '#A2A2A2', fontSize: '12px', fontWeight: '500'}}>Topics</span>
            {topics.map((topic, index)=>{
                return <ClassTestTopic key={index} topic={topic} />
            })}
        </Grid>
    </div>
    )
}
