import React from 'react'
import { storagePath } from '../../../constants'
import { Grid } from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import InfoIcon from '@material-ui/icons/Info';

export default function ChapterDetailList(props){
  console.log('chapterDetail props ->', props)
  return(
    <Grid container  className="chapter-detail">
      <p> school</p>
      <Grid item xs={12}>
        <h5 style={props.topic.learning_type === 2 && props.topic.status === 0 ? {} : {margin: '13px 0 3px 0'}}>
          {props.topic.title}
          {props.topic.learning_type === 2 && props.topic.status === 0 ?
              <img style={{float: 'right', width: '10px'}} src={`${storagePath}lock.svg`} alt={props.topic.title}/>
            :null
          }
        </h5>
      </Grid>
      <Grid item xs={12}>
          {props.topic.status !== 0 ?
          <ul class="progressbar">
              <li>
                {
                  props.topic.digital_card_status === 1 ?
                    <CheckCircleIcon style={{background: 'white'}}/>
                  :<div className="not-checked-circle"></div>
                }
                <p>Learning</p>
              </li>
              <li className={props.topic.quiz_detail[1] === 2 ? 'failed' : ''}>
                {
                  props.topic.quiz_detail[1] === 1?
                    <CheckCircleIcon style={{background: 'white'}}/>
                  :props.topic.quiz_detail[1] === 2 ?
                    <InfoIcon/>
                  :<div className="not-checked-circle"></div>
                }
                <p>Quiz Level 1 </p> {props.topic.quiz_detail[1] && props.topic.quiz_detail[1] !== 0 ? <a onClick={() => props.getStudentQuiz(1)}>(View Results)</a> : null}
              </li>
              <li className={props.topic.quiz_detail[2] === 2 ? 'failed' : ''}>
                {
                  props.topic.quiz_detail[2] === 1 ?
                    <CheckCircleIcon style={{background: 'white'}}/>
                  :props.topic.quiz_detail[2] === 2 ?
                    <InfoIcon/>
                  :<div className="not-checked-circle"></div>
                }
                <p>Quiz Level 2</p>{props.topic.quiz_detail[2] && props.topic.quiz_detail[2] !== 0 ? <a onClick={() => props.getStudentQuiz(2)}>(View Results)</a> : null}
              </li>
              {
                props.type === 'post' ?
                <li className={props.topic.quiz_detail[3] === 2 ? 'failed' : ''}>
                  {
                    props.topic.quiz_detail[3] === 1 ?
                      <CheckCircleIcon style={{background: 'white'}}/>
                    :props.topic.quiz_detail[3] === 2?
                      <InfoIcon/>
                    :<div className="not-checked-circle"></div>
                  }
                  <p>Quiz Level 3</p>{props.topic.quiz_detail[3] && props.topic.quiz_detail[3] !== 0 ? <a onClick={() => props.getStudentQuiz(3)}>(View Results)</a> : null}
                </li>
                :null
              }
          </ul> : null }
      </Grid>
    </Grid>
  )
}
