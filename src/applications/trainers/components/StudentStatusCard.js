import React from 'react'
import { Grid, Avatar, Button } from '@material-ui/core'
export default function StudentStatusCard(props){
  console.log(props);
    return(
        <Grid item xs={12} className="student-status-card" style={props.style}>
            {
              props.type != 'post' ?
              <Grid item style={{marginRight: '15px', cursor: 'pointer'}}>
                {
                  props.student.profile_pic ?
                    <Avatar onClick={props.redirectTo} className="student-image" src={props.student.profile_pic}></Avatar>
                  :<Avatar onClick={props.redirectTo} className="student-image">{props.student.first_name.substr(0,1)}{props.student.last_name ? props.student.last_name.substr(0,1) : ''}</Avatar>
                }
              </Grid>
              :null
            }
            <Grid item xs={props.type == 'post' ? 12 : 10} className="student-info">
                {
                  props.type == 'post' ?
                  <div style={{marginBottom: '10px', cursor: 'pointer'}}>
                    {
                      props.student.profile_pic ?
                        <Avatar onClick={props.redirectTo} style={{width: 'auto'}} className="student-image" src={props.student.profile_pic}></Avatar>
                      :<Avatar onClick={props.redirectTo} style={{width: 'auto'}} className="student-image">{props.student.first_name.substr(0,1)}{props.student.last_name ? props.student.last_name.substr(0,1) : ''}</Avatar>
                    }
                  </div>
                  :null
                }
                <h3 onClick={props.redirectTo} style={{cursor: 'pointer'}}>{props.student.first_name} {props.student.last_name}</h3>
                <h4>{props.student.roll_number}</h4>
                {props.status.props.children=="Learning"?'': <p><b> Attempts Completed : {props.student.no_of_attempts} </b></p> } 
                {props.status == "Need Help" ? <Button variant="contained" onClick={props.helpStudent} size="small" className="help-btn">Help</Button> : props.status}
                {props.quizFlag ? <span onClick={props.getStudentQuiz}>View Results</span> : null}

            </Grid>
        </Grid>
    )
}
