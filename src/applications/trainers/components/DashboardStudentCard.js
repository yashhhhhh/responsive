import React from 'react'
import { Grid, Avatar } from '@material-ui/core'
export default function DashboardStudentCard(props){
    // console.log('student', props.student)
    const { student } = props
    return(
    <Grid item xs={12} className="dashboard-student" onClick={props.redirectTo} style={{cursor: 'pointer'}}>
        <Grid item style={{marginRight: '10px'}}>
            { student.profile_pic ?
                <Avatar src={student.profile_pic}></Avatar>
                :<Avatar className="student-image">{student.full_name ? student.full_name.substr(0,1) : ''}</Avatar>
            }
        </Grid>
        <Grid item className="student-info">
            <h3>{student.full_name ? student.full_name : student.first_name ? student.first_name : ''}{student.last_name ? student.last_name : ''}</h3>
            <p>{student.roll_number}</p>
            {student.status ? <span>"{student.status}"</span> :null}
        </Grid>
    </Grid>)
}
