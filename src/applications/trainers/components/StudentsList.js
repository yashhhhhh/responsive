import React from 'react'
import Grid from "@material-ui/core/Grid"
import Avatar from '@material-ui/core/Avatar';
import { storagePath } from '../../../constants'
import moment from 'moment'

export default function StudentsList(props){
  return(
      <Grid container className="student-list"  onClick={() => props.getStudentProfile(props.student.id)}>
          <Grid item md={3} xs={12}>
          <div className="student-detail">
            {
              props.student.profile_pic ?
              <Avatar className="student-image" src={props.student.profile_pic}></Avatar>
              :<Avatar className="student-image">{props.student.first_name.substr(0,1)}{props.student.last_name ? props.student.last_name.substr(0,1) : ''}</Avatar>
            }
              <div className="student-info">
                <h3>{props.student.first_name ? props.student.first_name : ''} {props.student.last_name ? props.student.last_name : ''}</h3>
                <p>{props.student.roll_number ? props.student.roll_number : ''}</p>
              </div>
            </div>
          </Grid>
          <Grid item md={2} xs={12}>
            <p>Last Login</p>
            <h4>{props.student.last_login ? moment(props.student.last_login).format('MMM DD, YYYY, HH:mm') : '--'}</h4>
          </Grid>
          <Grid item md={2} xs={12}>
          <p>Avg. Daily Time</p>
          <h4>{props.student.avg_daily_usage ? `${props.student.avg_daily_usage} Mins` : '--'}</h4>
          </Grid>
          <Grid item md={5} xs={12}>
            <div className="learning-status">
                <p>Current Status</p>
                <h4>
                  {props.student.current_status ? `${props.student.current_status}` : '--'}
                </h4>
            </div>
          </Grid>
          <Grid item md={3} xs={12} style={{display: 'none'}}>
          <div className="upschool-progress">
            <p>UpSchool Score</p>
            <span className="warning-image" style={{display: 'none'}}>
              <img src ={storagePath + "warning.svg"} alt="warning" />
              <span>2 Items</span>
            </span>
            <div>
                <div className="progress">
                  <div className="progress-success"></div>
                </div>
                <div>
                  <span>0 out 10</span>
                </div>
           </div>
          </div>
        </Grid>
      </Grid>
  )
}
