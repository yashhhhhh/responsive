import React from 'react'
export default function DashboardStatus(props){
    return(
        <ul className="learning-status-card">
            {props.data.map((item, index)=>{
               return <li key={index}>
                    <p>{item.label}</p>
                    <h3>{item.value}</h3>
                </li>
            })}
        </ul>
    )
}