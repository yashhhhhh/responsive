import React from 'react'

export default function AnnouncementCard(props){
    return(
        <div className="announcement-card-wrapper">
            <p>Posted on {props.data.publishDate}</p>
            <h3>{props.data.content}</h3>
            <p>{props.data.status.viewed} Students have viewed. {props.data.status.notViewed} Students have not read the updated!</p>
        </div>
    )
}