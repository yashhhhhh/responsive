import React from 'react'
export default function LearningProgressBar({status}){
    return(
        <React.Fragment>
            <div className="learning-progress-bar" style={status.performers == 0 && status.performers == 0 && status.below_average == 0 ? {border: '1px solid #D6D8E1'} : {}}>
                <div className="progress performers" style={{width: `${status.performers}%`}}></div>
                <div  className="progress average" style={{width: `${status.performers + status.average}%`}}></div>
                <div  className="progress below-average" style={{width: status.below_average ? status.performers + status.average + status.below_average + '%' : '0%'}}></div>
            </div>
            <ul className="learning-progress-status">
                <li>
                    <div className="status-circle performers"></div>
                    <span>Performers ( {status.performers > 0 ? parseFloat(status.performers).toFixed(2) : 0}% )</span>
                </li>
                <li>
                    <div className="status-circle average"></div>
                    <span>Average ( {status.average > 0 ? parseFloat(status.average).toFixed(2) : 0}% )</span>
                </li>
                <li>
                    <div className="status-circle below-average"></div>
                    <span>Below Average ( {status.below_average > 0 ? parseFloat(status.below_average).toFixed(2) : 0}% )</span>
                </li>
            </ul>
        </React.Fragment>
    )
}
