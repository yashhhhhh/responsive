import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Button } from '@material-ui/core'
export default function ChapterSummaryTable(props) {
  const postTableHead = ["Learning", "Quiz Level 1", "Quiz Level 2", 'Quiz Level 3', "Need Attention"]
  const preTableHead = ["Learning", "Quiz Level 1", "Quiz Level 2"]
  return (
    <Table className="summary-table">
      <TableHead>
        <TableRow>
          <TableCell>{props.tableHead}</TableCell>
          {
            props.type === 'pre' ?
              preTableHead.map((item, index) => {
                return <TableCell key={index}>{item}</TableCell>
              }) : postTableHead.map((item, index) => {
                return <TableCell key={index}>{item}</TableCell>
              })
          }
        </TableRow>
      </TableHead>
      <TableBody>
        {
          props.chapterTopics && props.chapterTopics.length ?
            props.chapterTopics.map((topic, index) =>
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  <span style={{ display: 'block', width: '-webkit-fill-available' }} onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null}>{topic.title}</span>
                  {
                    props.type === 'post' && topic.status === 0 ?
                      <Button variant="contained" onClick={() => props.handleTopicModal(topic)} size="small" className="btn">Unlock</Button>
                      : null
                  }
                  <ul>
                    <li onClick={() => { props.handleTopicOpen(topic.id); props.getDigicard(topic.id); }}>View DigiCards</li>
                  </ul>
                </TableCell>
                <TableCell onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null} align="center">{topic.students.learning}</TableCell>
                <TableCell onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null} align="center">{topic.students.quiz[1]}</TableCell>
                <TableCell onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null} align="center">{topic.students.quiz[2]}</TableCell>
                {
                  props.type === 'post' ?
                    <TableCell onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null} align="center">{topic.students.quiz[3]}</TableCell>
                    : null
                }
                {
                  props.type === 'post' ?
                    <TableCell onClick={(e) => props.handleTopicOpen ? props.handleTopicOpen(topic.id) : null} align="center">{topic.students.need_attention ? topic.students.need_attention : 0}</TableCell>
                    : null
                }
              </TableRow>
            )
            : null
        }
      </TableBody>
    </Table>
  )
}
