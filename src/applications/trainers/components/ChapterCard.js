import React from 'react'
import { Grid, Card, CardContent, CardActions, Typography, Button, Tooltip } from '@material-ui/core'
import { storagePath } from '../../../constants'

export default function ChapterCard(props){
  return(
    <Grid item xs={4}>
      <Card className="chapter-card">
        <CardContent>
          <Typography color="textSecondary" gutterBottom className="chapter-number">
            Chapter {props.chapter.index}
            <img style={{float: 'right'}} src={`${storagePath}${props.chapter.status !== 0 ? 'unlock.svg' : 'lock.svg'}`} alt={props.chapter.title}/>
          </Typography>
          <Typography variant="h6" component="h6">
            {
              props.chapter.title.length > 35 ?
                <Tooltip title={props.chapter.title}>
                  <span>{props.chapter.title.substr(0,32)}...</span>
                </Tooltip>
              :`${props.chapter.title}`
            }
          </Typography>
          <CardActions>
            {
              props.chapter.status === 0 ?
              <Button variant="contained" onClick={props.unlockChapter} size="small" className="btn">Unlock</Button>
              :<Button variant="contained" onClick={props.viewChapter} size="small" className="btn">View</Button>
            }
            {
              props.chapter.status !== 0 && props.chapter.progress > 0 ?
              <div className="progress-con">
                <span>Status ({Math.round(props.chapter.progress)}% Completed)</span>
                  <div className="progress">
                    <div className="progress-success" style={{width: "100%", background: `linear-gradient(90deg, #96B375 ${props.chapter.progress}%, #E3E3E3 0%)`}}></div>
                  </div>
              </div>
              :null
            }
          </CardActions>
        </CardContent>
      </Card>
    </Grid>)
}
