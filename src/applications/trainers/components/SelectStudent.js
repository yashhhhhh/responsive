import React, { Fragment } from 'react'
import { Grid, FormControl, Select, OutlinedInput, MenuItem, Link } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons'
export default function SelectStudent(props){
    return(
        <Fragment>
            <Link className="primary-link" onClick={()=>{props.handleStudentOpen('')}} style={{marginBottom:'8px'}}><ArrowBack style={{verticalAlign: 'sub'}}/>Back</Link>
            <Grid container>
                <Grid item xs={3}>
                <FormControl className="select-item">
                    <Select
                        value={props.value}
                        onChange={props.handleStudentChange}
                        input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                    >
                    {props.students.map((student, index) => {
                        return <MenuItem key={index} value={student.id}>{student.name}</MenuItem>
                    })}
                    </Select>
                </FormControl>
                </Grid>
        </Grid>
        </Fragment>

    )
}
