import React from 'react'
import { Grid } from '@material-ui/core'
import MathJax from 'react-mathjax-preview-ds'
export default function QuizAnswerOption(props){
    return(
        <Grid item xs={props.col} className={props.className}>
            <span>{props.title} {props.attempted ? '(Attempted)' : ''}</span>
            <MathJax math={props.value}></MathJax>
        </Grid>
    )
}
