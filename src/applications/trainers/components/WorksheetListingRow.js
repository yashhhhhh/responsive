import React from 'react'
import { TableRow, TableCell } from '@material-ui/core';
import { Link } from 'react-router-dom'
import moment from 'moment'
export default function WorksheetListingRow({row, downloadSheet}){
  const statuses = [
    "",
    'On Going',
    "Scheduled",
    "Completed",
  ]
    return(
        <TableRow>
            <TableCell style={{width: '50%'}}><p>Chapter {row.chapter.index} : {row.chapter.title}</p><h3>{row.title}</h3></TableCell>
            <TableCell>{row.total_questions}</TableCell>
            <TableCell>{moment(row.created_at).format('DD MMM YYYY')}</TableCell>
            <TableCell>{row.submit_date ? moment(row.submit_date).format('DD MMM YYYY') : '--'}</TableCell>
            <TableCell align='right' style={{paddingRight:'0px'}}>
              <Link style={{marginRight: '10px', display: 'none'}} >VIEW</Link>
              <Link style={{marginRight: '10px'}} onClick={() => downloadSheet()}>Download</Link>
            </TableCell>
        </TableRow>
    )
}
