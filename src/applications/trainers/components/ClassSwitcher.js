import React, { useState } from 'react'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import { storagePath } from '../../../constants'
import { withRouter } from 'react-router-dom'

function ClassSwitcher(props){
  const defaultStandard = {} // JSON.parse(localStorage.getItem('syllabus'))
  const standards = props.standards
  const [classInfo, selectClass] = useState(defaultStandard.standard)
  const handleClassChange = event => {
    selectClass(event.target.value);
  };
  const user = JSON.parse(localStorage.getItem('userInfo')).user
  return(
    <div>
      <img src={user.school.logo} style={{width: '50px'}} alt="school"/>
      <div style={{marginRight: '10px'}}>
        <p>You are accessing</p>
        <h4 style={{display: 'block', marginLeft: '60px'}}>{user.school.name}</h4>
        <span style={{marginLeft: '60px'}}>{user.school.address}</span>
      </div>
        <FormControl className="select-class-container">
          <Select
            value={classInfo}
            onChange={handleClassChange}
            input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
          >
          {
            standards.standards ?
            standards.standards.map((standard, index) =>
              <MenuItem key={index} value={standard.id}>{standard.name}</MenuItem>
            ):null
          }
          </Select>
        </FormControl>
    </div>
  )
}

export default withRouter(ClassSwitcher)
