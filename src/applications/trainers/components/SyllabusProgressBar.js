import React from 'react'
import ErrorIcon from '@material-ui/icons/Error';
export default function SyllabusProgressBar({status}){
    return(
        <React.Fragment>
            <div className="learning-progress-bar">
                <div className="progress performers" style={{background: status.complete <=100 ? `linear-gradient(90deg, #96B375 ${status.complete}%, #E3E3E3 0%)`  : '#E3E3E3', width: '80%'}}>

                </div>
                <span style={{float:'right'}}><ErrorIcon color="disabled"/></span>
            </div>
            <p>{`${status.complete <=100 ? parseFloat(status.complete).toFixed(2) : 0}% complete`}</p>
        </React.Fragment>
    )
}
