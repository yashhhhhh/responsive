import React from 'react'
import { Grid } from '@material-ui/core'
export default function ClassTestTopic(props){
    return(
        <Grid item>
          <span class="arrow-left"></span>
          <div className="classTest-topic">
            {props.topic.title} ({props.topic.no_of_objective_questions + props.topic.no_of_subjective_questions})
          </div>
        </Grid>
    )
}
