import { globalGetService, globalPostService, globalPutService, globalDownloadGetService, globalDeleteService } from '../../global_api_services'
import { getStore } from '../../store/globalStore'
import { SHOW_TOAST } from '../auth/actions'
import moment from 'moment'

export function getConstants(type) {
  globalGetService(`/get-constants/?constant_type=${type}`)
    .then(response => {
      this.setState({
        classTestTypes: response.data.data
      })
    })
}

export function enableChapter(chapter, section, postDate) {
  this.setState({ loader: true })
  globalPostService(`teacher/section/${section}/chapter/${chapter}/unlock/`, { post_topic_unlock_date: postDate })
    .then(response => {
      this.setState({
        chapterUnlock: false
      })
      let store = getStore();
      store.dispatch({
        type: SHOW_TOAST,
        payload: {
          message: response.data.message,
          type: 'success',
          flag: true
        }
      });
      this.getSyllabus()
    })
}
export function enableTopic(chapter, section, postDate) {
  this.setState({ loader: true, chapterUnlock: false })
  globalPostService(`teacher/section/${section}/topic/${chapter}/unlock/`, { deadline_date: postDate })
    .then(response => {
      this.setState({
        chapterUnlock: false,
        postLearningDate: new Date(),
      })
      let store = getStore();
      store.dispatch({
        type: SHOW_TOAST,
        payload: {
          message: response.data.message,
          type: 'success',
          flag: true
        }
      });
      this.getTopicsList()
    })
}
export function getSyllabus() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/chapters/`)
    .then(response => {
      this.setState({
        syllabus: response.data.data
      })
      this.setState({ loader: false })
    })
}
export function getStudentsList(queryParams) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/students/${queryParams}`)
    .then(response => {
      this.setState({ students: response.data.data })
      this.setState({ loader: false })
    })
}
export function getTopicsList() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/chapter/${this.props.match.params.chapterSlug}/topics/`)
    .then(response => {
      this.setState({ chapterDetail: response.data.data })
      this.setState({ loader: false })
    })
}

export function getTopicDetails(chapter, topic) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/chapter/${chapter}/topic/${topic}/`)
    .then(response => {
      this.setState({
        topicStudents: response.data.data.topic.students
      })
      this.setState({ loader: false })
    })
}

export function getDigicard(topic) {
  this.setState({ loader: true })
  globalGetService(`/teacher/topic/${topic}/digital-cards/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ viewCard: true, cardList: response.data.data })
      }
      this.setState({ loader: false })
    })
}

export function getCardDetail(topic, card) {
  this.setState({ loader: true })
  globalGetService(`/teacher/topic/${topic}/digital-card/${card}/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ cardDetail: { detail: response.data.data, flag: true } })
      }
      this.setState({ loader: false })
    })
}

export function getNeedAttenstionList() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/need-attention/`)
    .then(response => {
      this.setState({ students: response.data.data })
      this.getCloseAttenstionList()
    })
}

export function getCloseAttenstionList() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/need-attention/closed/`)
    .then(response => {
      this.setState({ closedStudents: response.data.data })
      this.setState({ loader: false })
    })
}

export function helpStudentTopic(chapter, object) {
  if (object.remarks.trim() === '') {
    this.setState({ error: { remarks: 'Please enter remarks' } })
    return
  }
  this.setState({ loader: true })
  this.setState({ helpStudent: { flag: false, student: {} }, remarks: '' })
  globalPostService(`/teacher/chapter/${chapter}/unlock-need-attention-quiz/`, object)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.getNeedAttenstionList()
        this.getCloseAttenstionList()
      }
      this.setState({ loader: false })
    })
}

export function getStudentstatuses(chapter, type) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/section/${standardInfo.section}/chapter/${chapter}/?learning_type=${type}`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ chapterStatus: response.data.data })
      }
    })
}

export function getStudentQuiz(params) {
  this.setState({ loader: true })
  globalPostService(`/teacher/topic/${params.topic}/student/${params.student_id}/`, { quiz_type: params.quiz_type })
    .then(response => {
      this.setState({ quizData: { ...response.data.data, flag: true } })
      this.setState({ loader: false })
    })
}


export function getTestChapters(params) {
  const standardInfo = {} //JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/chapters/`, params)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          chapters: response.data.data
        })
      }
    })
}

export function getClassTestList(type) {
  this.setState({ loader: true })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-tests/`, type)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          tests: {
            classTests: response.data.data.class_tests,
            pagination: response.data.data.pagination
          }
        })
      }
      globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/students/?dropdown=true`)
        .then(response => {
          this.setState({
            students: response.data.data
          })
        })
      this.setState({ loader: false })
    })
}

export function getWorksheetList(filters) {
  this.setState({ loader: true })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/work-sheets/`, filters)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          tests: {
            classTests: response.data.data.work_sheets,
            pagination: response.data.data.pagination
          }
        })
      }
      globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/students/?dropdown=true`)
        .then(response => {
          this.setState({
            students: response.data.data
          })
        })
      this.setState({ loader: false })
    })
}

export function createClassTest(object) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  let classTest = object
  if (!classTest.schedule) {
    delete classTest.scheduled_date_time
  } else {
    classTest = { ...classTest, scheduled_date_time: moment.utc(classTest.scheduled_date_time).format('YYYY-MM-DD HH:mm:ss') }
  }
  if (object.mode === 'edit') {
    globalPutService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/${object.id}/`, classTest)
      .then(response => {
        let store = getStore();
        if (response.data.statusCode === 200) {
          store.dispatch({
            type: SHOW_TOAST,
            payload: {
              message: response.data.message,
              type: 'success',
              flag: true
            }
          });
          this.getClassTestList()
        }
        this.setState({ loader: false })
      })
  } else {
    globalPostService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/create/`, classTest)
      .then(response => {
        let store = getStore();
        if (response.data.statusCode === 200) {
          store.dispatch({
            type: SHOW_TOAST,
            payload: {
              message: response.data.message,
              type: 'success',
              flag: true
            }
          });
          this.getClassTestList()
        }
        this.setState({ loader: false })
      })
  }
}

export function createWorksheet(object) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalPostService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/work-sheet/create/`, { ...object, submit_date: object.submit_date ? moment(object.submit_date).format("YYYY-MM-DD") : object.submit_date })
    .then(response => {
      let store = getStore();
      if (response.data.statusCode === 200) {
        store.dispatch({
          type: SHOW_TOAST,
          payload: {
            message: response.data.message,
            type: 'success',
            flag: true
          }
        });
        this.getWorksheetList()
      }
      this.setState({ loader: false })
    })
}

export function getClassTestDetail() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/${this.props.match.params.classTestSlug}/detail/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          testDetail: response.data.data
        })
        this.getClassTestStudents()
      }
      this.setState({ loader: false })
    })
}

export function getClassTestStudents() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/${this.props.match.params.classTestSlug}/student-detail/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          students: response.data.data
        })
      }
      this.setState({ loader: false })
    })
}

export function getStudentSyllabus() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/student/${this.props.match.params.studentSlug}/syllabus/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          syllabus: response.data.data.syllabus,
          student: response.data.data.student
        })
      }
      this.setState({ loader: false })
    })
}

export function getStudentTest() {
  this.setState({ loader: true })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/student/${this.props.match.params.studentSlug}/class-test/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          classTests: response.data.data.class_test
        })
      }
      this.setState({ loader: false })
    })
}

export function getStudentActions() {
  this.setState({ loader: true })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`/teacher/student/${this.props.match.params.studentSlug}/actions/`, { subject_id: standardInfo.subject })
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({
          activity: response.data.data
        })
      }
      this.setState({ loader: false })
    })
}

export function getDashboardLearning() {
  this.setState({ loader: true })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/dashboard/section/${standardInfo.section}/subject/${standardInfo.subject}/learning-status/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ learningStatus: response.data.data })
      }
      this.setState({ loader: false })
    })
}

export function getDashboardSyllabusStatus() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/dashboard/section/${standardInfo.section}/subject/${standardInfo.subject}/syllabus-status/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ syllabusStatus: response.data.data })
      }
    })
}

export function getDashboardStudentStatus() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/dashboard/section/${standardInfo.section}/student-status/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ studentStatus: response.data.data })
      }
    })
}

export function getDashboardChapterStatus() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/dashboard/section/${standardInfo.section}/subject/${standardInfo.subject}/pending-chapter/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ pendingChapters: response.data.data })
      }
    })
}

export function getDashboardAttentionStudents() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/need-attention/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ neetAttention: response.data.data })
      }
    })
}

export function getDashboardTopPerformers() {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalGetService(`teacher/dashboard/section/${standardInfo.section}/subject/${standardInfo.subject}/top_performers/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ topPerformers: response.data.data })
      }
    })
}

export function getStudentTestDetail(classTest, student) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/${classTest}/student/${student}`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ quizData: { quiz: response.data.data.assessments, title: this.state.testDetail.title }, openStudentFlag: true })
      }
      this.setState({ loader: false })
    })
}

export function getWorkSheetDetail(id) {
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  this.setState({ loader: true })
  globalGetService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/work-sheet/${id}/detail/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.setState({ workSheet: response.data.data, modal: true })
      }
      this.setState({ loader: false })
    })
}

export function downloadWorkSheet(id) {
  this.setState({ loader: true })
  globalDownloadGetService(`teacher/worksheet/${id}/export-questions/`)
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `worksheet_${new Date}` + id + '.pdf');
      document.body.appendChild(link);
      link.click();
      this.setState({ loader: false })
    })
}

export function deleteClassTest(testId, type) {
  this.setState({ loader: true })
  this.setState({ deleteTest: { flag: false, title: '' } })
  const standardInfo = JSON.parse(localStorage.getItem('syllabus'))
  globalDeleteService(`teacher/section/${standardInfo.section}/subject/${standardInfo.subject}/class-test/${testId}/`)
    .then(response => {
      if (response.data.statusCode === 200) {
        this.getClassTestList(type)
      }
      this.setState({ loader: false })
    })
}
