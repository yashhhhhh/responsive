import LoginForm from './containers/LoginForm'
import SelectClass from './containers/SelectClass'
import ForgotPassword from './containers/ForgotPassword'
import ResetPassword from './containers/ResetPassword'
import LoggedUserRed from '../../hocs/LoggedUserRedirect'

export default [
  {
    path: '/login',
    component: LoggedUserRed(LoginForm),
    key: 'login'
  },
  {
    path: '/',
    component: LoggedUserRed(LoginForm),
    key: 'home'
  },
  {
    path: '/forgot-password',
    component: LoggedUserRed(ForgotPassword),
    key: 'forgot_password'
  },
  {
    path: '/password/reset/:token',
    component: LoggedUserRed(ResetPassword),
    key: 'reset_password'
  },
  {
    path: '/select-class',
    component: SelectClass,
    key: 'select_class'
  },
]
