import { globalPostService, globalGetService } from '../../global_api_services'
import { getStore } from '../../store/globalStore'
import { SHOW_TOAST } from '../auth/actions'
export function loginApi(){
  this.setState({loader: true})
  globalPostService('api/login/', this.state.user)
  .then(response => {
    if(response.data.validation_error && Object.keys(response.data.validation_error).length){
      this.setState({
        error: response.data.validation_error
      })
    }else{
      window.localStorage.setItem('userInfo', JSON.stringify(response.data.data))
      this.props.history.push('/dashboard')
      // this.getUserClassApi()
    }
    this.setState({loader: false})
  })
}

export function getUserClassApi (){
  globalGetService('teacher/standards/')
  .then(response => {
    this.props.standardList(response.data.data)
    if(response.data.data.standards)
    if(response.data.data.standards.length === 1){
      this.setState({
        sections: response.data.data.standards[0].sections,
        classInfo: response.data.data.standards[0].id
      },() => {
        if(this.state.sections.length === 1){
          this.setState({
            sectionInfo: this.state.sections[0].id,
            subjects: this.state.sections[0].subjects,
            subjectInfo: this.state.sections[0].subjects[0].id
          },() => {
            if(this.state.subjects.length === 1){
              window.localStorage.setItem('syllabus', JSON.stringify({
                standard: this.state.classInfo,
                section: this.state.sectionInfo,
                subject: this.state.subjectInfo,
                subjectName: this.state.subjects.filter(subject => subject.id === this.state.subjectInfo)[0].name,
                sectionName: this.state.sections.filter(subject => subject.id === this.state.sectionInfo)[0].name,
                standardName: response.data.data.standards[0].name,
              }))
              window.location.assign('/dashboard')
            }else{
              this.props.history.push('/select-class')
            }
          })
        }else{
          this.props.history.push('/select-class')
        }
      })
    }else{
      this.props.history.push('/select-class')
    }
    this.setState({standardData: response.data.data})
  })
}

export function requestPasswordResetApi (email){
  this.setState({loader: true})
  globalPostService('api/password/reset/',{email})
  .then(response => {
    if(response.data.statusCode === 200){
      this.setState({showForm: false})
    }
    this.setState({loader: false})
  })
}

export function resetPasswordResetApi (user){
  this.setState({loader: true})
  globalPostService('api/password/reset-confirm/',user)
  .then(response => {
    if(response.data.statusCode === 200){
      let store = getStore();
      store.dispatch({
        type: SHOW_TOAST,
        payload: {
          message: response.data.message,
          type: 'success',
          flag: true
        }
      });
      setTimeout(() => this.props.history.push('/login'), 5000)
    }
    this.setState({loader: false})
  })
}
