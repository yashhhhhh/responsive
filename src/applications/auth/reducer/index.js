import * as actions from '../actions'
export default function(state={
  standards: [],
  toast:{
    message: '',
    type: '',
    flag: false
  }
}, action){
  switch (action.type) {
    case actions.STANDARDS_LIST:
      return {
        ...state,
        standards: action.payload
      }
    case actions.SHOW_TOAST:
      return {
        ...state,
        toast: action.payload
      }
    default:
      return state
  }
}
