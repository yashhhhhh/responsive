import React, { Component } from 'react'
import { InputLabel, Grid, Container } from '@material-ui/core'
import PrimaryButton from '../../../shared_elements/ui_elements/PrimaryButton'
import { storagePath } from '../../../constants'
import { isMobile } from 'react-device-detect';
import { Snackbar, IconButton } from '@material-ui/core'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { resetPasswordResetApi } from '../api_services'
import { withRouter } from 'react-router-dom'
import CloseIcon from '@material-ui/icons/Close';

class ResetPassword extends Component{
  constructor(props){
    super(props)
    this.state={
      loader: false,
      user:{
        confirm_password: '',
        password: ''
      },
      error:{}
    }

    this.resetPasswordResetApi = resetPasswordResetApi.bind(this)
  }

  updateForm = (key, value) =>{
    let error = this.state.error
    if(value.trim() === ''){
      return
    }
    delete error[key]
    this.setState({user: {...this.state.user, [key]: value}, error})
  }

  loginUser = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    let validateNewInput = {
      password: this.state.user.password.trim() === '' ? 'Please enter password' : '',
      confirm_password: this.state.user.confirm_password.trim() === '' ? 'Please confirm password' : '',
    }
    if(this.state.user.password.trim() !== '' && this.state.user.confirm_password.trim() !== '' &&this.state.user.password !== this.state.user.confirm_password){
      validateNewInput = {
        ...validateNewInput,
        password: 'Passswords do not match',
        confirm_password: ''
      }
    }

    if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === '' })){
      let slug = this.props.match.params.token.split('_')
      this.resetPasswordResetApi({...this.state.user, token: slug[1], uSlug: slug[0]})
    }else{
      this.setState({
        error: validateNewInput
      })
    }
  }

  render(){
    return(
      <div className="login-container" style={{background: "url('" + storagePath + "upschool_bg.png')", backgroundSize: 'cover'}}>
        <Container maxWidth="md">
          <Grid container style={{height: '-webkit-fill-available'}} alignItems="center" direction={isMobile ? "row" : "row-reverse"}>
            <Grid item xs={12} md={12}>
              <div className="logo">
                <img src={storagePath + 'logo_dark.png'} alt="upschool"/>
              </div>
            </Grid>
            <Grid item md={5} xs={12}>
              <img src={storagePath + 'login_image.png'} alt="UpSchool" id="login_image"/>
              <div className="home-message">
                "We uplift the quality of deliverance on and off classrooms.
                We offer a unique three pronged approach to Evaluate, Educate, and Elevate a student."
              </div>
            </Grid>
            <Grid item md={7} xs={12}>
              <div className="welcome-note">
                <h3>Reset Password</h3>
              </div>
              <div className="login-form">
                <form onSubmit={this.loginUser}>
                  <div>
                    <InputLabel htmlFor="username">Password<span className="required-asterisk">*</span></InputLabel>
                    <input
                        id="username"
                        className="username"
                        defaultValue=""
                        type="password"
                        placeholder="Enter email"
                        onChange={(evt) => this.updateForm('password', evt.target.value)}
                      />
                    <h6 className="error">{this.state.error.password}</h6>
                  </div>
                  <div>
                    <InputLabel htmlFor="username">Confirm Password<span className="required-asterisk">*</span></InputLabel>
                    <input
                        id="username"
                        className="username"
                        defaultValue=""
                        type="password"
                        placeholder="Enter email"
                        onChange={(evt) => this.updateForm('confirm_password', evt.target.value)}
                      />
                    <h6 className="error">{this.state.error.confirm_password}</h6>
                  </div>
                  <PrimaryButton type="submit" disabled={this.state.loader} onClick={this.loginUser} value={this.state.loader ? "Processing..." : "Reset Password" }className="login-button"></PrimaryButton>
                </form>
              </div>
            </Grid>

          </Grid>
        </Container>
        <Snackbar
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={this.props.toast.flag}
            autoHideDuration={4500}
            onClose={this.props.closeToast}
            className={this.props.toast.type === "error" ? "warning-toast" : "success-toast"}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.props.toast.message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="close"
                color="inherit"
                onClick={this.props.closeToast}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
      </div>
    )
  }
}

const mapStateToProps = (state) =>({
  standards: state.AuthReducer.standards,
  toast: state.AuthReducer.toast,
})
const mapDispatchToProps = (dispatch) =>{
  return{
    closeToast: () => dispatch({
      type: actions.SHOW_TOAST,
      payload: {
        message: '',
        type: '',
        flag: false
      }
    })
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResetPassword))
