import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { InputLabel, Grid, Container, FormControl, Select, MenuItem, OutlinedInput } from '@material-ui/core'
import PrimaryButton from '../../../shared_elements/ui_elements/PrimaryButton'
import { storagePath } from '../../../constants'
import { getUserClassApi } from '../api_services'
import * as actions from '../actions'
class SelectClass extends Component{
  constructor(props){
    super(props)
    this.state = {
      classInfo: '',
      sectionInfo: '',
      subjectInfo: '',
      standardData: [],
      sections: [],
      subjects: [],
      error: {}
    }

    this.getUserClassApi = getUserClassApi.bind(this)
  }

  componentDidMount(){
    this.getUserClassApi()
  }

  handleClassChange = (evt) => {
    this.setState({
      classInfo: evt.target.value,
      sectionInfo: evt.target.value === this.state.classInfo ? this.state.sectionInfo : '',
      subjectInfo: evt.target.value === this.state.classInfo ? this.state.subjectInfo : '',
      error: {...this.state.error, classInfo:'' }
    },() => {
      this.state.standardData.standards.map(standard => {
        if(standard.id === this.state.classInfo){
          this.setState({
            sections: standard.sections
          },() => {
            if(this.state.sections.length === 1){
              this.setState({
                sectionInfo: this.state.sections[0].id,
                subjects: this.state.sections[0].subjects
              },() => {
                if(this.state.subjects.length === 1){
                  window.localStorage.setItem('syllabus', JSON.stringify({
                    standard: this.state.classInfo,
                    section: this.state.sectionInfo,
                    subject: this.state.subjectInfo,
                    subjectName: this.state.subjects.filter(subject => subject.id === this.state.subjectInfo)[0].name,
                    sectionName: this.state.sections.filter(subject => subject.id === this.state.sectionInfo)[0].name,
                    standardName: this.state.standardData.standards.filter(subject => subject.id === this.state.classInfo)[0].name,
                  }))
                  window.location.assign('/dashboard')
                }
              })
            }
          })
        }
        return standard
      })
    })
  }
  handleSectionChange = (evt) => {
    this.setState({
      sectionInfo: evt.target.value,
      subjectInfo: evt.target.value === this.state.sectionInfo ? this.state.subjectInfo : '',
      error: {...this.state.error, sectionInfo:'' }
    },() => {
      if(this.state.sections.length === 1){
        this.setState({
          sectionInfo: this.state.sections[0].id,
          subjects: this.state.sections[0].subjects,
        }, () => {
          if(this.state.subjects.length === 1){
            window.localStorage.setItem('syllabus', JSON.stringify({
              standard: this.state.classInfo,
              section: this.state.sectionInfo,
              subject: this.state.subjectInfo,
              subjectName: this.state.subjects.filter(subject => subject.id === this.state.subjectInfo)[0].name,
              sectionName: this.state.sections.filter(subject => subject.id === this.state.sectionInfo)[0].name,
              standardName: this.state.standardData.standards.filter(subject => subject.id == this.state.classInfo)[0].name,
            }))
            window.location.assign('/dashboard')
          }
        })
      }else{
        this.state.sections.map(section => {
          if(section.id === this.state.sectionInfo){
            this.setState({
              subjects: section.subjects
            },() =>{
              if(this.state.subjects.length === 1){
                this.setState({
                  subjectInfo: this.state.subjects[0].id
                })
              }
            })
          }
          return section
        })
      }
    })
  }


  handleSubjectChange = (evt) => {
    this.setState({subjectInfo: evt.target.value, error: {...this.state.error, subjectInfo:'' }})
  }

  selectClass = (evt) => {
    evt.preventDefault()
    evt.stopPropagation()
    let validateNewInput = {
      classInfo: this.state.classInfo === '' ? 'Please select class' : '',
      sectionInfo: this.state.sectionInfo === '' ? 'Please select section' : '',
      subjectInfo: this.state.subjectInfo === '' ? 'Please select subject' : '',
    }
    if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === '' })){
      window.localStorage.setItem('syllabus', JSON.stringify({
        standard: this.state.classInfo,
        section: this.state.sectionInfo,
        subject: this.state.subjectInfo,
        subjectName: this.state.subjects.filter(subject => subject.id === this.state.subjectInfo)[0].name,
        sectionName: this.state.sections.filter(subject => subject.id === this.state.sectionInfo)[0].name,
        standardName: this.state.standardData.standards.filter(subject => subject.id === this.state.classInfo)[0].name,
      }))
      window.location.assign('/dashboard')
    }else{
      this.setState({
        error: validateNewInput
      })
    }
  }

  render(){
    const user = JSON.parse(localStorage.getItem('userInfo')).user
    return(
      <div className="login-container" style={{background: "url('" + storagePath + "upschool_bg.png')", backgroundSize: 'cover'}}>
        <Container maxWidth="md" style={{height: '100%'}}>
          <Grid container style={{height: '-webkit-fill-available'}} alignItems="center" direction="column" justify="center">
            <Grid item xs={12} md={12}>
              <div className="logo">
                <img src={storagePath + 'logo_dark.png'} alt="upschool"/>
              </div>
            </Grid>
            <Grid item xs={6} className="class-switcher">
              <div className="select-class">
                <form onSubmit={this.selectClass}>
                  <InputLabel>Class<span className="required-asterisk">*</span></InputLabel>
                  <FormControl className="select-class-container">
                    <Select
                      value={this.state.classInfo}
                      onChange={this.handleClassChange}
                      input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                    >
                    {
                      this.state.standardData.standards ?
                      this.state.standardData.standards.map((standard, index) =>
                        <MenuItem key={index} value={standard.id}>{standard.name}</MenuItem>
                      ):null
                    }
                    </Select>
                    <h6 className="error">{this.state.error.classInfo}</h6>
                  </FormControl>
                  <InputLabel>Section<span className="required-asterisk">*</span></InputLabel>
                  <FormControl className="select-class-container">
                    <Select
                      value={this.state.sectionInfo}
                      onChange={this.handleSectionChange}
                      input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                    >
                    {
                      this.state.sections ?
                      this.state.sections.map((section, index) =>
                        <MenuItem key={index + "_section"} value={section.id}>{section.name}</MenuItem>
                      ):null
                    }
                    </Select>
                    <h6 className="error">{this.state.error.sectionInfo}</h6>
                  </FormControl>
                  <InputLabel>Subject<span className="required-asterisk">*</span></InputLabel>
                  <FormControl className="select-class-container">
                    <Select
                      value={this.state.subjectInfo}
                      onChange={this.handleSubjectChange}
                      input={<OutlinedInput name="class" id="outlined-age-native-simple" />}
                    >
                    {
                      this.state.subjects ?
                      this.state.subjects.map((subject, index) =>
                        <MenuItem key={index + "_subject"} value={subject.id}>{subject.name}</MenuItem>
                      ):null
                    }
                    </Select>
                    <h6 className="error">{this.state.error.subjectInfo}</h6>
                  </FormControl>
                  <PrimaryButton type="submit" onClick={this.selectClass} value="Continue"/>
                </form>
              </div>
            </Grid>
            <Grid item xs={6} className="class-switcher">
              <div className="select-class-auth">
                <h3 className="welcome-note">Welcome Back <br/><strong>{user.full_name}</strong></h3>
                <div className="school-info">
                  <img src={user.school.logo} alt="school" style={{width: '50px'}}/>
                  <div className="school-detail">
                    <p>You are accessing</p>
                    <h4>{user.school.name}</h4>
                    <span>{user.school.address}</span>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) =>({
  standards: state.AuthReducer.standards
})
const mapDispatchToProps = (dispatch) =>{
  return{
    standardList:(data) => dispatch({
      type: actions.STANDARDS_LIST,
      payload: data
    })
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SelectClass))
