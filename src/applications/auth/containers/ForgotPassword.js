import React, { Component } from 'react'
import { InputLabel, Grid, Container } from '@material-ui/core'
import PrimaryButton from '../../../shared_elements/ui_elements/PrimaryButton'
import { storagePath } from '../../../constants'
import { isMobile } from 'react-device-detect';
import { requestPasswordResetApi } from '../api_services'
import { withRouter } from 'react-router-dom'

class ForgotPassword extends Component{
  constructor(props){
    super(props)
    this.state={
      loader: false,
      user:{
        username: '',
        password: ''
      },
      error:{},
      showForm: true
    }

    this.requestPasswordResetApi = requestPasswordResetApi.bind(this)
  }

  updateForm = (key, value) =>{
    let error = this.state.error
    if(value.trim() === ''){
      return
    }
    delete error[key]
    this.setState({user: {...this.state.user, [key]: value}, error})
  }

  loginUser = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    let validateNewInput = {
      username: this.state.user.username.trim() === '' ? 'Please enter email' : '',
    }
    if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === '' })){
      this.requestPasswordResetApi(this.state.user.username)
    }else{
      this.setState({
        error: validateNewInput
      })
    }
  }

  render(){
    return(
      <div className="login-container" style={{background: "url('" + storagePath + "upschool_bg.png')", backgroundSize: 'cover'}}>
        <Container maxWidth="md">
          <Grid container style={{height: '-webkit-fill-available'}} alignItems="center" direction={isMobile ? "row" : "row-reverse"}>
            <Grid item xs={12} md={12}>
              <div className="logoes">
                <img src={storagePath + 'logo_dark.png'} alt="upschool"/>
              </div>
            </Grid>
            <Grid item md={5} xs={12}>
              <img src={storagePath + 'login_image.png'} alt="UpSchool" id="login_image"/>
              <div className="home-message">
                "We uplift the quality of deliverance on and off classrooms.
                We offer a unique three pronged approach to Evaluate, Educate, and Elevate a student."
              </div>
            </Grid>
            <Grid item md={7} xs={12}>
              {
                this.state.showForm ?
                <div className="welcome-note">
                  <h3>Forgot Password ?</h3>
                  <p>Enter email below to get password reset link</p>
                </div>
                :null
              }
              <div className="login-form" >
                {
                  this.state.showForm ?
                  <form onSubmit={this.loginUser}>
                    <div>
                      <InputLabel htmlFor="username">Email<span className="required-asterisk">*</span></InputLabel>
                      <input
                          id="username"
                          className="username"
                          defaultValue=""
                          placeholder="Enter email"
                          onChange={(evt) => this.updateForm('username', evt.target.value)}
                        />
                      <h6 className="error">{this.state.error.username}</h6>
                    </div>
                    <PrimaryButton type="submit" disabled={this.state.loader} onClick={this.loginUser} value={this.state.loader ? "Processing..." : "Request Password" }className="login-button"></PrimaryButton>
                  </form>
                  :<div className="success-message">
                    <img src={storagePath + "thumb_up.png"} alt="success"/>
                    <p>We have sent an email, with instruction to reset password</p>
                  </div>
                }
              </div>
            </Grid>

          </Grid>
        </Container>
      </div>
    )
  }
}

export default withRouter(ForgotPassword)
