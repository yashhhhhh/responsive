import React, { Component } from 'react'
import { InputLabel, Grid, Container } from '@material-ui/core'
import PrimaryButton from '../../../shared_elements/ui_elements/PrimaryButton'
import { storagePath } from '../../../constants'
import { isMobile } from 'react-device-detect';
import { connect } from 'react-redux'
import { loginApi, getUserClassApi } from '../api_services'
import { withRouter, Link } from 'react-router-dom'
import * as actions from '../actions'

class LoginForm extends Component{
  constructor(props){
    super(props)
    this.state={
      loader: false,
      user:{
        username: '',
        password: ''
      },
      error:{}
    }

    this.loginApi = loginApi.bind(this)
    this.getUserClassApi = getUserClassApi.bind(this)
  }

  updateForm = (key, value) =>{
    let error = this.state.error
    if(value.trim() === ''){
      return
    }
    delete error[key]
    this.setState({user: {...this.state.user, [key]: value}, error})
  }

  componentWillMount(){
    if(localStorage.getItem('userInfo')){
      if(!localStorage.getItem('syllabus')){
        this.props.history.push('/select-class')
      }
    }
  }

  loginUser = (evt) =>{
    evt.preventDefault()
    evt.stopPropagation()
    let validateNewInput = {
      username: this.state.user.username.trim() === '' ? 'Please enter username' : '',
      password: this.state.user.password.trim() === '' ? 'Please enter password' : '',
    }
    if (Object.keys(validateNewInput).every((k) => { return validateNewInput[k] === '' })){
      this.loginApi()
    }else{
      this.setState({
        error: validateNewInput
      })
    }
  }

  render(){
    return(
      <div className="login-container" style={{background: "url('" + storagePath + "upschool_bg.png')", backgroundSize: 'cover'}}>
        <Container maxWidth="md">
          <Grid container style={{height: '-webkit-fill-available'}} alignItems="center" direction={isMobile ? "row" : "row-reverse"} style={{ minHeight: '100vh' }}>
            
            <Grid item md={5} xs={12}>
              <img src={storagePath + 'login_image.png'} alt="UpSchool" id="login_image"/>
              <div className="home-message">
                "We uplift the quality of deliverance on and off classrooms.
                We offer a unique three pronged approach to Evaluate, Educate, and Elevate a student."
              </div>
            </Grid>
            <Grid item md={7} xs={12}>
              <div className="logos">
                <img src={storagePath + 'logo_dark.png'} alt="upschool"/>
              </div>
              <div className="welcome-note">
                <h3>Welcome Back</h3>
                <p>Login via Principal account to proceed</p>
              </div>
              <div className="login-form">
                <form onSubmit={this.loginUser}>
                  <div>
                    <InputLabel htmlFor="username">Username<span className="required-asterisk">*</span></InputLabel>
                    <input
                        id="username"
                        className="username"
                        defaultValue=""
                        placeholder="Enter username"
                        onChange={(evt) => this.updateForm('username', evt.target.value)}
                      />
                    <h6 className="error">{this.state.error.username}</h6>
                  </div>
                  <div>
                    <InputLabel htmlFor="password">Password<span className="required-asterisk">*</span></InputLabel>
                    <input
                        id="password"
                        className="password"
                        defaultValue=""
                        type="password"
                        placeholder="Enter password"
                        onChange={(evt) => this.updateForm('password', evt.target.value)}
                      />
                    <h6 className="error">{this.state.error.password}</h6>
                  </div>
                  <div className="notice">By clicking Login, you agree to our <a href="https://up.school/terms-and-conditions/" target="_blank">Terms & Conditions</a> and <a href="https://up.school/privacy-policy/" target="_blank">Privacy Policy</a></div>
                  <PrimaryButton type="submit" disabled={this.state.loader} onClick={this.loginUser} value={this.state.loader ? "Processing..." : "Login" }className="login-button"></PrimaryButton>
                </form>
              <span className="forgot-password"><Link className="primary-link" to="/forgot-password">Forgot Password ?</Link></span>
              </div>
            </Grid>

          </Grid>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) =>({
  standards: state.AuthReducer.standards
})
const mapDispatchToProps = (dispatch) =>{
  return{
    standardList:(data) => dispatch({
      type: actions.STANDARDS_LIST,
      payload: data
    })
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginForm))
